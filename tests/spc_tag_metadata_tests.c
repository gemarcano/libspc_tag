#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <spc_tag/spc_tag_metadata.h>
#include <spc_tag_tests.h>

#ifndef DATA_DIR
#error "Don't know where to get unit test data from! Define DATA_DIR to the directory with unit test data."
#endif

struct test_data
{
	spc_tag_metadata *metadata;
	spc_tag_metadata *written;
	FILE *bad1;
	FILE *bad2;
	FILE *text;
	FILE *binary;
	FILE *temp;
};

#define xstr(s) str(s)
#define str(s) #s

void spc_tag_metadata_init(void **test_data)
{
	*test_data = malloc(sizeof(struct test_data));
	struct test_data *tdata = *test_data;
	tdata->metadata = spc_tag_metadata_new();
	tdata->written= spc_tag_metadata_new();
	tdata->bad1 = fopen(xstr(DATA_DIR) "/bad1.spc", "rb");
	tdata->bad2 = fopen(xstr(DATA_DIR) "/bad2.spc", "rb");
	tdata->text = fopen(xstr(DATA_DIR) "/text.spc", "rb");
	tdata->binary = fopen(xstr(DATA_DIR) "/binary.spc", "rb");
	tdata->temp = tmpfile();
}

void spc_tag_metadata_cleanup(void *test_data)
{
	struct test_data *data = test_data;
	spc_tag_metadata_free(data->metadata);
	spc_tag_metadata_free(data->written);
	fclose(data->bad1);
	fclose(data->bad2);
	fclose(data->text);
	fclose(data->binary);
	fclose(data->temp);
	free(test_data);
}

ST_TEST_DEFINE(metadata_parse)
{
	struct test_data *tdata = test_data;
	spc_tag_metadata *metadata = tdata->metadata;
	ST_EQ(spc_tag_metadata_parse_file(tdata->bad1, metadata), SPC_TAG_PARSE_ERR);
	ST_EQ(spc_tag_metadata_parse_file(tdata->bad2, metadata), SPC_TAG_IO_ERR);
	ST_EQ(spc_tag_metadata_parse_file(NULL, metadata), SPC_TAG_IO_ERR);

	// I made a custom file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	// Check basic properties of xid666, let the more in-depth testing happen
	// in the xid666 unit tests
	ST_EQ(spc_tag_metadata_parse_file(tdata->text, metadata), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_EQ(spc_tag_metadata_get_has_id666(metadata), true);
	spc_tag_xid666 *xid666 = spc_tag_metadata_get_xid666(metadata);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_EQ(spc_tag_metadata_get_binary(metadata), false);
	ST_STRNEQ(spc_tag_metadata_get_song_title(metadata), "Test file", 32);
	ST_STRNEQ(spc_tag_metadata_get_game_title(metadata), "Fake Game", 32);
	ST_STRNEQ(
		spc_tag_metadata_get_name_of_dumper(metadata), "Gabriel Marcano", 16);
	ST_STRNEQ(spc_tag_metadata_get_comments(metadata), "Fake comment", 32);
	struct tm date = spc_tag_metadata_get_dump_date(metadata);
	ST_EQ(date.tm_year, 120);
	ST_EQ(date.tm_mon, 5);
	ST_EQ(date.tm_mday, 7);
	ST_EQ(spc_tag_metadata_get_seconds_until_fade(metadata), 123);
	ST_EQ(spc_tag_metadata_get_fade_length_ms(metadata), 12345);
	ST_STRNEQ(spc_tag_metadata_get_song_artist(metadata), "Nobody", 32);
	ST_EQ(spc_tag_metadata_get_default_channel_disables(metadata), false);
	ST_EQ(spc_tag_metadata_get_emulator_used_to_dump(metadata), 0);

	// Make sure to free resources before re-using the metadata memory space
	spc_tag_metadata_destructor(metadata);
	spc_tag_metadata_constructor(metadata);

	// Now check binary parsing, all data SHOULD be the same
	ST_EQ(spc_tag_metadata_parse_file(tdata->binary, metadata), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_get_has_id666(metadata), true);
	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	xid666 = spc_tag_metadata_get_xid666(metadata);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_EQ(spc_tag_metadata_get_binary(metadata), true);
	ST_STRNEQ(spc_tag_metadata_get_song_title(metadata), "Test file", 32);
	ST_STRNEQ(spc_tag_metadata_get_game_title(metadata), "Fake Game", 32);
	ST_STRNEQ(
		spc_tag_metadata_get_name_of_dumper(metadata), "Gabriel Marcano", 16);
	ST_STRNEQ(spc_tag_metadata_get_comments(metadata), "Fake comment", 32);
	date = spc_tag_metadata_get_dump_date(metadata);
	ST_EQ(date.tm_year, 120);
	ST_EQ(date.tm_mon, 5);
	ST_EQ(date.tm_mday, 7);
	ST_EQ(spc_tag_metadata_get_seconds_until_fade(metadata), 123);
	ST_EQ(spc_tag_metadata_get_fade_length_ms(metadata), 12345);
	ST_STRNEQ(spc_tag_metadata_get_song_artist(metadata), "Nobody", 32);
	ST_EQ(spc_tag_metadata_get_default_channel_disables(metadata), false);
	ST_EQ(spc_tag_metadata_get_emulator_used_to_dump(metadata), 0);

	spc_tag_metadata_destructor(metadata);
	return "ok";
}

ST_TEST_DEFINE(metadata_parse_memory)
{
	struct test_data *tdata = test_data;
	spc_tag_metadata *metadata = tdata->metadata;

	// Allocate enough space for all files
	unsigned char *buffer = malloc(100000);
	size_t amount, size;
	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->bad1));
		size += amount);
	ST_EQ(spc_tag_metadata_parse_memory(buffer, size, metadata),
			SPC_TAG_PARSE_ERR);
	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->bad2));
		size += amount);
	ST_EQ(spc_tag_metadata_parse_memory(buffer, size, metadata), SPC_TAG_IO_ERR);
	ST_EQ(spc_tag_metadata_parse_memory(NULL, size, metadata), SPC_TAG_IO_ERR);

	// I made a custom file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	// Check basic properties of xid666, let the more in-depth testing happen
	// in the xid666 unit tests
	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->text));
		size += amount);
	ST_EQ(spc_tag_metadata_parse_memory(buffer, size, metadata),
			SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_EQ(spc_tag_metadata_get_has_id666(metadata), true);
	spc_tag_xid666 *xid666 = spc_tag_metadata_get_xid666(metadata);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_EQ(spc_tag_metadata_get_binary(metadata), false);
	ST_STRNEQ(spc_tag_metadata_get_song_title(metadata), "Test file", 32);
	ST_STRNEQ(spc_tag_metadata_get_game_title(metadata), "Fake Game", 32);
	ST_STRNEQ(
		spc_tag_metadata_get_name_of_dumper(metadata), "Gabriel Marcano", 16);
	ST_STRNEQ(spc_tag_metadata_get_comments(metadata), "Fake comment", 32);
	struct tm date = spc_tag_metadata_get_dump_date(metadata);
	ST_EQ(date.tm_year, 120);
	ST_EQ(date.tm_mon, 5);
	ST_EQ(date.tm_mday, 7);
	ST_EQ(spc_tag_metadata_get_seconds_until_fade(metadata), 123);
	ST_EQ(spc_tag_metadata_get_fade_length_ms(metadata), 12345);
	ST_STRNEQ(spc_tag_metadata_get_song_artist(metadata), "Nobody", 32);
	ST_EQ(spc_tag_metadata_get_default_channel_disables(metadata), false);
	ST_EQ(spc_tag_metadata_get_emulator_used_to_dump(metadata), 0);

	// Make sure to free resources before re-using the metadata memory space
	spc_tag_metadata_destructor(metadata);
	spc_tag_metadata_constructor(metadata);

	// Now check binary parsing, all data SHOULD be the same
	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->binary));
		size += amount);
	ST_EQ(spc_tag_metadata_parse_memory(buffer, size, metadata),
			SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_get_has_id666(metadata), true);
	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	xid666 = spc_tag_metadata_get_xid666(metadata);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_EQ(spc_tag_metadata_get_binary(metadata), true);
	ST_STRNEQ(spc_tag_metadata_get_song_title(metadata), "Test file", 32);
	ST_STRNEQ(spc_tag_metadata_get_game_title(metadata), "Fake Game", 32);
	ST_STRNEQ(
		spc_tag_metadata_get_name_of_dumper(metadata), "Gabriel Marcano", 16);
	ST_STRNEQ(spc_tag_metadata_get_comments(metadata), "Fake comment", 32);
	date = spc_tag_metadata_get_dump_date(metadata);
	ST_EQ(date.tm_year, 120);
	ST_EQ(date.tm_mon, 5);
	ST_EQ(date.tm_mday, 7);
	ST_EQ(spc_tag_metadata_get_seconds_until_fade(metadata), 123);
	ST_EQ(spc_tag_metadata_get_fade_length_ms(metadata), 12345);
	ST_STRNEQ(spc_tag_metadata_get_song_artist(metadata), "Nobody", 32);
	ST_EQ(spc_tag_metadata_get_default_channel_disables(metadata), false);
	ST_EQ(spc_tag_metadata_get_emulator_used_to_dump(metadata), 0);

	spc_tag_metadata_destructor(metadata);
	free(buffer);
	return "ok";
}

ST_TEST_DEFINE(metadata_write)
{
	struct test_data *tdata = test_data;
	spc_tag_metadata *metadata = tdata->metadata;

	// Check with a binary SPC,  that we can read, write, and read it back in
	// again
	ST_EQ(spc_tag_metadata_parse_file(tdata->binary, metadata), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_write_file(tdata->temp, metadata), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_parse_file(tdata->temp, tdata->written), SPC_TAG_SUCCESS);

	ST_ACCESSOR_EQ(spc_tag_metadata, has_id666, metadata, tdata->written);
	spc_tag_xid666 *xid666 = spc_tag_metadata_get_xid666(metadata);
	spc_tag_xid666 *wxid666 = spc_tag_metadata_get_xid666(tdata->written);
	//ST_ACCESSOR_STRNEQ(spc_tag_xid666, type, xid666, xid666, 4);
	ST_ACCESSOR_EQ(spc_tag_xid666, size, xid666, wxid666);
	ST_ACCESSOR_EQ(spc_tag_xid666, chunks, xid666, wxid666);

	ST_ACCESSOR_EQ(
		spc_tag_metadata, minor_version, metadata, tdata->written);
	ST_ACCESSOR_EQ(spc_tag_metadata, binary, metadata, tdata->written);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, song_title, metadata, tdata->written, 32);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, game_title, metadata, tdata->written, 32);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, name_of_dumper, metadata, tdata->written, 16);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, comments, metadata, tdata->written, 32);
	struct tm date = spc_tag_metadata_get_dump_date(metadata);
	struct tm wdate = spc_tag_metadata_get_dump_date(tdata->written);
	ST_EQ(date.tm_year, wdate.tm_year);
	ST_EQ(date.tm_mon, wdate.tm_mon);
	ST_EQ(date.tm_mday, wdate.tm_mday);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, seconds_until_fade, metadata, tdata->written);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, fade_length_ms, metadata, tdata->written);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, song_artist, metadata, tdata->written, 32);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, default_channel_disables, metadata,
			tdata->written);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, emulator_used_to_dump, metadata, tdata->written);
	spc_tag_metadata_destructor(tdata->written);
	spc_tag_metadata_constructor(tdata->written);
	spc_tag_metadata_destructor(metadata);
	spc_tag_metadata_constructor(metadata);

	// Now check with a text SPC
	ST_EQ(spc_tag_metadata_parse_file(tdata->text, metadata), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_write_file(tdata->temp, metadata), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_metadata_parse_file(tdata->temp, tdata->written), SPC_TAG_SUCCESS);

	ST_ACCESSOR_EQ(spc_tag_metadata, has_id666, metadata, tdata->written);
	xid666 = spc_tag_metadata_get_xid666(metadata);
	wxid666 = spc_tag_metadata_get_xid666(tdata->written);
	//ST_ACCESSOR_STRNEQ(spc_tag_xid666, type, xid666, xid666, 4);
	ST_ACCESSOR_EQ(spc_tag_xid666, size, xid666, wxid666);
	ST_ACCESSOR_EQ(spc_tag_xid666, chunks, xid666, wxid666);

	ST_ACCESSOR_EQ(
		spc_tag_metadata, minor_version, metadata, tdata->written);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, binary, metadata, tdata->written);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, song_title, metadata, tdata->written, 32);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, game_title, metadata, tdata->written, 32);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, name_of_dumper, metadata, tdata->written, 16);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, comments, metadata, tdata->written, 32);
	date = spc_tag_metadata_get_dump_date(metadata);
	wdate = spc_tag_metadata_get_dump_date(tdata->written);
	ST_EQ(date.tm_year, wdate.tm_year);
	ST_EQ(date.tm_mon, wdate.tm_mon);
	ST_EQ(date.tm_mday, wdate.tm_mday);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, seconds_until_fade, metadata, tdata->written);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, fade_length_ms, metadata, tdata->written);
	ST_ACCESSOR_STRNEQ(
		spc_tag_metadata, song_artist, metadata, tdata->written, 32);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, default_channel_disables, metadata,
			tdata->written);
	ST_ACCESSOR_EQ(
		spc_tag_metadata, emulator_used_to_dump, metadata, tdata->written);

	return "ok";
}

ST_TEST_DEFINE(metadata_accessors)
{
	struct test_data *tdata = test_data;
	spc_tag_metadata *metadata = tdata->metadata;
	// I made a custom file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	// Check basic properties of xid666, let the more in-depth testing happen
	// in the xid666 unit tests
	ST_EQ(spc_tag_metadata_parse_file(tdata->text, metadata), SPC_TAG_SUCCESS);

	ST_EQ(spc_tag_metadata_get_has_id666(metadata), true);

	ST_EQ(spc_tag_metadata_get_binary(metadata), false);
	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 31);
	ST_STRNEQ(spc_tag_metadata_get_song_title(metadata), "Test file", 32);
	ST_STRNEQ(spc_tag_metadata_get_game_title(metadata), "Fake Game", 32);
	ST_STRNEQ(
		spc_tag_metadata_get_name_of_dumper(metadata), "Gabriel Marcano", 16);
	ST_STRNEQ(spc_tag_metadata_get_comments(metadata), "Fake comment", 32);
	struct tm date = spc_tag_metadata_get_dump_date(metadata);
	ST_EQ(date.tm_year, 120);
	ST_EQ(date.tm_mon, 5);
	ST_EQ(date.tm_mday, 7);
	ST_EQ(spc_tag_metadata_get_seconds_until_fade(metadata), 123);
	ST_EQ(spc_tag_metadata_get_fade_length_ms(metadata), 12345);
	ST_STRNEQ(spc_tag_metadata_get_song_artist(metadata), "Nobody", 32);
	ST_EQ(spc_tag_metadata_get_default_channel_disables(metadata), false);
	ST_EQ(spc_tag_metadata_get_emulator_used_to_dump(metadata), 0);

	// No need to check xid666 in detail, just check that it exists and is
	// valid
	spc_tag_xid666 *xid = spc_tag_metadata_get_xid666(metadata);
	//ST_STRNEQ(xid->type, "xid6", 4);
	ST_EQ(spc_tag_xid666_get_chunks(xid), 6);

	// Test that we can set values...
	spc_tag_metadata_set_has_id666(metadata, false);
	spc_tag_metadata_set_binary(metadata, true);
	spc_tag_metadata_set_minor_version(metadata, 30);
	spc_tag_metadata_set_song_title(metadata, "Test file2", 4);
	spc_tag_metadata_set_game_title(metadata, "Fake Game", 3);
	spc_tag_metadata_set_name_of_dumper(metadata, "Gabriel Marcano", 2);
	spc_tag_metadata_set_comments(metadata, "Fake comment", 1);
	date = (struct tm){.tm_year = 100, .tm_mon = 0, .tm_mday = 1};
	spc_tag_metadata_set_dump_date(metadata, date);
	spc_tag_metadata_set_seconds_until_fade(metadata, 100);
	spc_tag_metadata_set_fade_length_ms(metadata, 10001);
	spc_tag_metadata_set_song_artist(metadata, "Nobody", 3);
	spc_tag_metadata_set_default_channel_disables(metadata, true);
	spc_tag_metadata_set_emulator_used_to_dump(metadata, 1);

	// ... And read them back
	ST_EQ(spc_tag_metadata_get_has_id666(metadata), false);
	ST_EQ(spc_tag_metadata_get_binary(metadata), true);
	ST_EQ(spc_tag_metadata_get_minor_version(metadata), 30);
	ST_STRNEQ(spc_tag_metadata_get_song_title(metadata), "Test", 32);
	ST_STRNEQ(spc_tag_metadata_get_game_title(metadata), "Fak", 32);
	ST_STRNEQ(spc_tag_metadata_get_name_of_dumper(metadata), "Ga", 16);
	ST_STRNEQ(spc_tag_metadata_get_comments(metadata), "F", 32);
	date = spc_tag_metadata_get_dump_date(metadata);
	ST_EQ(date.tm_year, 100);
	ST_EQ(date.tm_mon, 0);
	ST_EQ(date.tm_mday, 1);
	ST_EQ(spc_tag_metadata_get_seconds_until_fade(metadata), 100);
	ST_EQ(spc_tag_metadata_get_fade_length_ms(metadata), 10001);
	ST_STRNEQ(spc_tag_metadata_get_song_artist(metadata), "Nob", 32);
	ST_EQ(spc_tag_metadata_get_default_channel_disables(metadata), true);
	ST_EQ(spc_tag_metadata_get_emulator_used_to_dump(metadata), 1);

	return "ok";
}

spc_tag_test tests[] = {
	{.name = "metadata_parse", .init = spc_tag_metadata_init, .function =
		spc_tag_metadata_parse_test, .cleanup = spc_tag_metadata_cleanup},
	{.name = "metadata_parse_memory", .init = spc_tag_metadata_init, .function =
		spc_tag_metadata_parse_memory_test, .cleanup = spc_tag_metadata_cleanup},
	{.name = "metadata_write", .init = spc_tag_metadata_init, .function =
		spc_tag_metadata_write_test, .cleanup = spc_tag_metadata_cleanup},
	{.name = "metadata_accessors", .init = spc_tag_metadata_init, .function =
		spc_tag_metadata_accessors_test, .cleanup = spc_tag_metadata_cleanup},
};

size_t count = sizeof(tests)/sizeof(*tests);

int main()
{
	for (size_t i = 0; i < count; ++i)
	{
		spc_tag_tests_register(&tests[i]);
	}

	int result = spc_tag_tests_run();
	spc_tag_tests_free();
	return result;
}
