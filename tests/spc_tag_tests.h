// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#ifndef SPC_TAG_TESTS_H_
#define SPC_TAG_TESTS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Struct representing a single unit test
typedef struct spc_tag_test
{
	// Display name of the test
	const char* name;

	// Code to run to initialize the test. It will be passed a pointer to a
	// void pointer stored in this object (the data variable), for it to set to
	// something for data storage. It is cleanup's responsibility to free
	// whatever resources are acquired here.
	void (*init)(void **test_data);
	// The actual test function. It is passed the data variable and teh
	// description variable, Users should only concern themselves with the
	// passed in test_data (same variable as the *test_data from init).
	// description is used by the testing harness to store the result of
	// failures.
	const char*(*function)(void *test_data, char *description, size_t desc_size);
	// Cleanup resources acquired during init.
	void (*cleanup)(void *test_data);

	// Pointer to data allocated in init
	void *data;
	// Descriptiong of test success/failure
	char description[256];
} spc_tag_test;

/** Registers a new test to run.
 *
 * @param[in] test Test structure to copy and register to run.
 */
void spc_tag_tests_register(spc_tag_test *test);

/** Runs all registered tests.
 */
int spc_tag_tests_run(void);

/** Frees all internally allocated data. If any tests are registered, this must
 * be called before the application ends.
 */
void spc_tag_tests_free(void);

// Declares and defines a new test. Should be used in place of the function
// prototype. It behavess as though it declares a function with the following
// prototype:
//   const char* spc_tag_ ## NAME ## _test(void *test_data)
//
// Users of the test framework should not use the description and desc_size
// parameters of the test function.
#define ST_TEST_DEFINE(NAME) \
const char * spc_tag_ ## NAME ##  _test(void *test_data, char *description, size_t desc_size)

// Test for equality. Sets the description to a failure if they're not equal
// and stops running the test.
#define ST_EQ(A, B)\
	if ((A) != (B)) { \
		snprintf(description, desc_size, __FILE__":%d: %s", __LINE__, #A " does not equal " #B); \
		return "not ok"; \
	}

// Test for string equality, given a length to compare up to (like strncmp).
// Sets the description to a failure if they're not equal and stops running the
// test.
#define ST_STRNEQ(A, B, S)\
	if (strncmp(A, B, S)) { \
		snprintf(description, desc_size, __FILE__":%d: %s", __LINE__, #A " does not equal " #B); \
		return "not ok"; \
	}

// Test for memory block equality, given a length to compare up to (like
// memcmp). Sets the description to a failure if they're not equal and stops
// running the test.
#define ST_MEMCMP(A, B, S)\
	if (memcmp(A, B, S)) { \
		snprintf(description, desc_size, __FILE__":%d: %s", __LINE__, #A " does not equal " #B); \
		return "not ok"; \
	}

// Shorthand for testing a NS_get_PROP() function (e.g. spc_tag_get_has_id666)
// Param A is the object to pass to the function, B is the value to test
// against
#define ST_ACCESSOR_VAL_EQ(NS, PROP, A, B) \
	ST_EQ(NS##_get_##PROP(A), B)

// Shorthand to test a property of two objects against each other.
#define ST_ACCESSOR_EQ(NS, PROP, A, B) \
	ST_ACCESSOR_VAL_EQ(NS, PROP, A, NS##_get_##PROP(B))

// Shorthand to test the string returned by a property of one object with the
// given string of length N.
#define ST_ACCESSOR_VAL_STRNEQ(NS, PROP, A, B, N) \
	ST_STRNEQ(NS##_get_##PROP(A), B, N)

// Shorthand to test the strings returned by the properties of two objects with
// a length of N.
#define ST_ACCESSOR_STRNEQ(NS, PROP, A, B, N) \
	ST_ACCESSOR_VAL_STRNEQ(NS, PROP, A, NS##_get_##PROP(B), N)

// Shorthand to test the memory returned by a property of one object with the
// given memory block of length N.
#define ST_ACCESSOR_VAL_MEMCMP(NS, PROP, A, B, N) \
	ST_MEMCMP(NS##_get_##PROP(A), B, N)

// Shorthand to test the memory returned by the properties of two objects with
// a length of N.
#define ST_ACCESSOR_MEMCMP(NS, PROP, A, B, N) \
	ST_ACCESSOR_VAL_MEMCMP(NS, PROP, A, NS##_get_##PROP(B), N)

#endif//SPC_TAG_TESTS_H_
