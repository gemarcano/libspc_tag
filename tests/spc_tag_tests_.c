#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <spc_tag/spc_tag.h>
#include <spc_tag_tests.h>

#ifndef DATA_DIR
#error "Don't know where to get unit test data from! Define DATA_DIR to the directory with unit test data."
#endif

struct test_data
{
	spc_tag *tag;
	spc_tag *written;
	FILE *bad1;
	FILE *bad2;
	FILE *text;
	FILE *binary;
	FILE *temp;
};

#define xstr(s) str(s)
#define str(s) #s

void spc_tag_init(void **test_data)
{
	*test_data = malloc(sizeof(struct test_data));
	struct test_data *tdata = *test_data;
	tdata->tag = spc_tag_new();
	tdata->written = spc_tag_new();
	tdata->bad1 = fopen(xstr(DATA_DIR) "/bad1.spc", "rb");
	tdata->bad2 = fopen(xstr(DATA_DIR) "/bad2.spc", "rb");
	tdata->text = fopen(xstr(DATA_DIR) "/text.spc", "rb");
	tdata->binary = fopen(xstr(DATA_DIR) "/binary.spc", "rb");
	tdata->temp = tmpfile();
}

void spc_tag_cleanup(void *test_data)
{
	struct test_data *data = test_data;
	spc_tag_free(data->tag);
	spc_tag_free(data->written);
	fclose(data->bad1);
	fclose(data->bad2);
	fclose(data->text);
	fclose(data->binary);
	fclose(data->temp);
	free(test_data);
}

ST_TEST_DEFINE(parse)
{
	struct test_data *tdata = test_data;
	spc_tag *data = tdata->tag;

	ST_EQ(spc_tag_parse_file(tdata->bad1, data), SPC_TAG_PARSE_ERR);
	ST_EQ(spc_tag_parse_file(tdata->bad2, data), SPC_TAG_IO_ERR);
	ST_EQ(spc_tag_parse_file(NULL, data), SPC_TAG_IO_ERR);

	// I made a custom tdata->file with data I wrote in with a hex editor, so
	// the test values were derived by me manually. Crosscheck that we parsed
	// what we expected
	// Check basic properties of xid666, let the more in-depth testing happen
	// in the xid666 unit tests
	ST_EQ(spc_tag_parse_file(tdata->text, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_has_id666(data), true);
	spc_tag_xid666 *xid666 = spc_tag_get_xid666(data);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_binary(data), false);

	// Check non-metadata fields
	ST_EQ(spc_tag_get_pc(data), 0x0201);
	ST_EQ(spc_tag_get_a(data), 3);
	ST_EQ(spc_tag_get_x(data), 4);
	ST_EQ(spc_tag_get_y(data), 5);
	ST_EQ(spc_tag_get_psw(data), 6);
	ST_EQ(spc_tag_get_sp(data), 7);

	// Just check some random locations in RAM variables
	ST_EQ(spc_tag_get_ram(data)[0], 0x11);
	ST_EQ(spc_tag_get_ram(data)[0x100], 0xA5);
	ST_EQ(spc_tag_get_dsp_regs(data)[0], 0x4A);
	ST_EQ(spc_tag_get_dsp_regs(data)[0x20], 0xD2);
	ST_EQ(spc_tag_get_extra_ram(data)[0], 0xA9);
	ST_EQ(spc_tag_get_extra_ram(data)[0x20], 0x45);

	// Make sure to free resources before re-using the data memory space
	spc_tag_destructor(data);
	spc_tag_constructor(data);

	// Now check binary parsing, all data SHOULD be the same
	ST_EQ(spc_tag_parse_file(tdata->binary, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_has_id666(data), true);
	xid666 = spc_tag_get_xid666(data);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_binary(data), true);

	// Check non-metadata fields
	ST_EQ(spc_tag_get_pc(data), 0x0201);
	ST_EQ(spc_tag_get_a(data), 3);
	ST_EQ(spc_tag_get_x(data), 4);
	ST_EQ(spc_tag_get_y(data), 5);
	ST_EQ(spc_tag_get_psw(data), 6);
	ST_EQ(spc_tag_get_sp(data), 7);

	// Just check some random locations in RAM variables
	ST_EQ(spc_tag_get_ram(data)[0], 0x11);
	ST_EQ(spc_tag_get_ram(data)[0x100], 0xA5);
	ST_EQ(spc_tag_get_dsp_regs(data)[0], 0x4A);
	ST_EQ(spc_tag_get_dsp_regs(data)[0x20], 0xD2);
	ST_EQ(spc_tag_get_extra_ram(data)[0], 0xA9);
	ST_EQ(spc_tag_get_extra_ram(data)[0x20], 0x45);
	spc_tag_destructor(data);
	return "ok";
}

ST_TEST_DEFINE(parse_memory)
{
	struct test_data *tdata = test_data;
	spc_tag *data = tdata->tag;

	// Allocate enough space for all files being tested
	unsigned char *buffer = malloc(100000);

	size_t amount, size;
	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->bad1));
		size += amount);
	ST_EQ(spc_tag_parse_memory(buffer, size, data), SPC_TAG_PARSE_ERR);

	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->bad2));
		size += amount);
	ST_EQ(spc_tag_parse_memory(buffer, size, data), SPC_TAG_IO_ERR);
	ST_EQ(spc_tag_parse_memory(NULL, size, data), SPC_TAG_IO_ERR);

	// I made a custom tdata->file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	// Check basic properties of xid666, let the more in-depth testing happen
	// in the xid666 unit tests
	for (size = 0;
		(amount = fread(buffer + size, 1, 1u<<16, tdata->text));
		size += amount);
	ST_EQ(spc_tag_parse_memory(buffer, size, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_has_id666(data), true);
	spc_tag_xid666 *xid666 = spc_tag_get_xid666(data);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_binary(data), false);

	// Check non-metadata fields
	ST_EQ(spc_tag_get_pc(data), 0x0201);
	ST_EQ(spc_tag_get_a(data), 3);
	ST_EQ(spc_tag_get_x(data), 4);
	ST_EQ(spc_tag_get_y(data), 5);
	ST_EQ(spc_tag_get_psw(data), 6);
	ST_EQ(spc_tag_get_sp(data), 7);

	// Just check some random locations in RAM variables
	ST_EQ(spc_tag_get_ram(data)[0], 0x11);
	ST_EQ(spc_tag_get_ram(data)[0x100], 0xA5);
	ST_EQ(spc_tag_get_dsp_regs(data)[0], 0x4A);
	ST_EQ(spc_tag_get_dsp_regs(data)[0x20], 0xD2);
	ST_EQ(spc_tag_get_extra_ram(data)[0], 0xA9);
	ST_EQ(spc_tag_get_extra_ram(data)[0x20], 0x45);

	// Make sure to free resources before re-using the data memory space
	spc_tag_destructor(data);
	spc_tag_constructor(data);

	// Now check binary parsing, all data SHOULD be the same
	for (size = 0; (amount = fread(buffer + size, 1, 1u<<16, tdata->binary)); size += amount);
	ST_EQ(spc_tag_parse_memory(buffer, size, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_has_id666(data), true);
	xid666 = spc_tag_get_xid666(data);
	//ST_STRNEQ(spc_tag_xid666_get_type(xid666), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(xid666), 248);
	ST_EQ(spc_tag_xid666_get_chunks(xid666), 6);

	ST_EQ(spc_tag_get_minor_version(data), 31);
	ST_EQ(spc_tag_get_binary(data), true);

	// Check non-metadata fields
	ST_EQ(spc_tag_get_pc(data), 0x0201);
	ST_EQ(spc_tag_get_a(data), 3);
	ST_EQ(spc_tag_get_x(data), 4);
	ST_EQ(spc_tag_get_y(data), 5);
	ST_EQ(spc_tag_get_psw(data), 6);
	ST_EQ(spc_tag_get_sp(data), 7);

	// Just check some random locations in RAM variables
	ST_EQ(spc_tag_get_ram(data)[0], 0x11);
	ST_EQ(spc_tag_get_ram(data)[0x100], 0xA5);
	ST_EQ(spc_tag_get_dsp_regs(data)[0], 0x4A);
	ST_EQ(spc_tag_get_dsp_regs(data)[0x20], 0xD2);
	ST_EQ(spc_tag_get_extra_ram(data)[0], 0xA9);
	ST_EQ(spc_tag_get_extra_ram(data)[0x20], 0x45);
	spc_tag_destructor(data);
	// Remember to free memory buffer
	free(buffer);
	return "ok";
}

ST_TEST_DEFINE(write)
{
	struct test_data *tdata = test_data;
	spc_tag *data = tdata->tag;

	// Check with a binary SPC,  that we can read, write, and read it back in
	// again
	ST_EQ(spc_tag_parse_file(tdata->binary, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_write_file(tdata->temp, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_parse_file(tdata->temp, tdata->written), SPC_TAG_SUCCESS);

	ST_ACCESSOR_EQ(spc_tag, has_id666, data, tdata->written);
	spc_tag_xid666 *xid666 = spc_tag_get_xid666(data);
	spc_tag_xid666 *wxid666 = spc_tag_get_xid666(tdata->written);
	//ST_ACCESSOR_STRNEQ(spc_tag_xid666, type, xid666, xid666, 4);
	ST_ACCESSOR_EQ(spc_tag_xid666, size, xid666, wxid666);
	ST_ACCESSOR_EQ(spc_tag_xid666, chunks, xid666, wxid666);

	ST_ACCESSOR_EQ(spc_tag, minor_version, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, binary, data, tdata->written);

	// Check non-metadata fields
	ST_ACCESSOR_EQ(spc_tag, pc, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, a, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, x, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, y, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, psw, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, sp, data, tdata->written);

	ST_ACCESSOR_MEMCMP(spc_tag, ram, data, tdata->written, 0x10000);
	ST_ACCESSOR_MEMCMP(spc_tag, dsp_regs, data, tdata->written, 128);
	ST_ACCESSOR_MEMCMP(spc_tag, extra_ram, data, tdata->written, 64);

	spc_tag_destructor(tdata->written);
	spc_tag_constructor(tdata->written);
	spc_tag_destructor(data);
	spc_tag_constructor(data);

	// Now check with a text SPC
	ST_EQ(spc_tag_parse_file(tdata->text, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_write_file(tdata->temp, data), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_parse_file(tdata->temp, tdata->written), SPC_TAG_SUCCESS);

	ST_ACCESSOR_EQ(spc_tag, has_id666, data, tdata->written);
	xid666 = spc_tag_get_xid666(data);
	wxid666 = spc_tag_get_xid666(tdata->written);
	//ST_ACCESSOR_STRNEQ(spc_tag_xid666, type, xid666, xid666, 4);
	ST_ACCESSOR_EQ(spc_tag_xid666, size, xid666, wxid666);
	ST_ACCESSOR_EQ(spc_tag_xid666, chunks, xid666, wxid666);

	ST_ACCESSOR_EQ(spc_tag, minor_version, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, binary, data, tdata->written);

	// Check non-metadata fields
	ST_ACCESSOR_EQ(spc_tag, pc, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, a, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, x, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, y, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, psw, data, tdata->written);
	ST_ACCESSOR_EQ(spc_tag, sp, data, tdata->written);

	ST_ACCESSOR_MEMCMP(spc_tag, ram, data, tdata->written, 0x10000);
	ST_ACCESSOR_MEMCMP(spc_tag, dsp_regs, data, tdata->written, 128);
	ST_ACCESSOR_MEMCMP(spc_tag, extra_ram, data, tdata->written, 64);

	return "ok";
}

ST_TEST_DEFINE(accessors)
{
	struct test_data *tdata = test_data;
	spc_tag *tag = tdata->tag;
	// I made a custom tdata->file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	ST_EQ(spc_tag_parse_file(tdata->text, tag), SPC_TAG_SUCCESS);

	// Check spc_tag unique data
	ST_EQ(spc_tag_get_pc(tag), 0x0201);
	ST_EQ(spc_tag_get_a(tag), 3);
	ST_EQ(spc_tag_get_x(tag), 4);
	ST_EQ(spc_tag_get_y(tag), 5);
	ST_EQ(spc_tag_get_psw(tag), 6);
	ST_EQ(spc_tag_get_sp(tag), 7);
	ST_EQ(spc_tag_get_reserved(tag), 0);
	const unsigned char *pointer = spc_tag_get_ram(tag);
	ST_EQ(pointer[0], 0x11);
	ST_EQ(pointer[0x5E73], 0xE3);
	ST_EQ(pointer[0xFFFF], 0x15);
	pointer = spc_tag_get_dsp_regs(tag);
	ST_EQ(pointer[0], 0x4A);
	ST_EQ(pointer[79], 0x96);
	ST_EQ(pointer[127], 0x9B);
	pointer = spc_tag_get_extra_ram(tag);
	ST_EQ(pointer[0], 0xA9);
	ST_EQ(pointer[46], 0x13);
	ST_EQ(pointer[63], 0xD4);

	// Now, check that we can set these values...
	ST_EQ(spc_tag_set_pc(tag, 0xFFFF), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_a(tag, 1), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_x(tag, 2), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_y(tag, 3), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_psw(tag, 4), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_sp(tag, 5), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_reserved(tag, 1), SPC_TAG_SUCCESS); // FIXME We shouldn't be messing with reserved values...
	unsigned char buffer[0x10000];
	for (size_t i = 0; i < 0x10000; ++i)
		buffer[i] = i;
	ST_EQ(spc_tag_set_ram(tag, buffer, 0x10000), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_dsp_regs(tag, buffer+1, 128), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_extra_ram(tag, buffer+2, 64), SPC_TAG_SUCCESS);

	// This shouldn't change the underlying values
	ST_EQ(spc_tag_set_ram(tag, buffer+1, 0x10001), SPC_TAG_PARSE_ERR);
	ST_EQ(spc_tag_set_dsp_regs(tag, buffer+2, 129), SPC_TAG_PARSE_ERR);
	ST_EQ(spc_tag_set_extra_ram(tag, buffer+3, 65), SPC_TAG_PARSE_ERR);

	// ... And now check the new values
	ST_EQ(spc_tag_get_pc(tag), 0xFFFF);
	ST_EQ(spc_tag_get_a(tag), 1);
	ST_EQ(spc_tag_get_x(tag), 2);
	ST_EQ(spc_tag_get_y(tag), 3);
	ST_EQ(spc_tag_get_psw(tag), 4);
	ST_EQ(spc_tag_get_sp(tag), 5);
	ST_EQ(spc_tag_get_reserved(tag), 1);
	pointer = spc_tag_get_ram(tag);
	ST_EQ(pointer[0], 0);
	ST_EQ(pointer[0x5E73], 0x73);
	ST_EQ(pointer[0xFFFF], 0xFF);
	pointer = spc_tag_get_dsp_regs(tag);
	ST_EQ(pointer[0], 1);
	ST_EQ(pointer[79], 80);
	ST_EQ(pointer[127], 128);
	pointer = spc_tag_get_extra_ram(tag);
	ST_EQ(pointer[0], 2);
	ST_EQ(pointer[46], 48);
	ST_EQ(pointer[63], 65);

	// FIXME should we check behavior for setting RAM and other arrays to
	// buffers less than the full size?

	// And now, check metadata -- we just forward these to the
	// spc_tag_metadata_* accessors internally, so just make sure that
	// forwarding works as intendend...
	ST_EQ(spc_tag_get_has_id666(tag), true);
	ST_EQ(spc_tag_get_binary(tag), false);
	ST_EQ(spc_tag_get_minor_version(tag), 31);
	ST_STRNEQ(spc_tag_get_song_title(tag), "Test file", 32);
	ST_STRNEQ(spc_tag_get_game_title(tag), "Fake Game", 32);
	ST_STRNEQ(spc_tag_get_name_of_dumper(tag), "Gabriel Marcano", 16);
	ST_STRNEQ(spc_tag_get_comments(tag), "Fake comment", 32);
	struct tm date = spc_tag_get_dump_date(tag);
	ST_EQ(date.tm_year, 120);
	ST_EQ(date.tm_mon, 5);
	ST_EQ(date.tm_mday, 7);
	ST_EQ(spc_tag_get_seconds_until_fade(tag), 123);
	ST_EQ(spc_tag_get_fade_length_ms(tag), 12345);
	ST_STRNEQ(spc_tag_get_song_artist(tag), "Nobody", 32);
	ST_EQ(spc_tag_get_default_channel_disables(tag), false);
	ST_EQ(spc_tag_get_emulator_used_to_dump(tag), 0);

	// No need to check xid666 in detail, just check that it exists and is
	// valid
	spc_tag_xid666 *xid = spc_tag_get_xid666(tag);
	//ST_STRNEQ(xid->type, "xid6", 4);
	ST_EQ(spc_tag_xid666_get_chunks(xid), 6);

	// Test that we can set values...
	ST_EQ(spc_tag_set_has_id666(tag, false), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_binary(tag, true), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_minor_version(tag, 30), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_song_title(tag, "Test file2", 4), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_game_title(tag, "Fake Game", 3), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_name_of_dumper(tag, "Gabriel Marcano", 2), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_comments(tag, "Fake comment", 1), SPC_TAG_SUCCESS);
	date = (struct tm){.tm_year = 100, .tm_mon = 0, .tm_mday = 1};
	ST_EQ(spc_tag_set_dump_date(tag, date), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_seconds_until_fade(tag, 100), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_fade_length_ms(tag, 10001), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_song_artist(tag, "Nobody", 3), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_default_channel_disables(tag, true), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_set_emulator_used_to_dump(tag, 1), SPC_TAG_SUCCESS);

	// ... And read them back
	ST_EQ(spc_tag_get_has_id666(tag), false);
	ST_EQ(spc_tag_get_binary(tag), true);
	ST_EQ(spc_tag_get_minor_version(tag), 30);
	ST_STRNEQ(spc_tag_get_song_title(tag), "Test", 32);
	ST_STRNEQ(spc_tag_get_game_title(tag), "Fak", 32);
	ST_STRNEQ(spc_tag_get_name_of_dumper(tag), "Ga", 16);
	ST_STRNEQ(spc_tag_get_comments(tag), "F", 32);
	date = spc_tag_get_dump_date(tag);
	ST_EQ(date.tm_year, 100);
	ST_EQ(date.tm_mon, 0);
	ST_EQ(date.tm_mday, 1);
	ST_EQ(spc_tag_get_seconds_until_fade(tag), 100);
	ST_EQ(spc_tag_get_fade_length_ms(tag), 10001);
	ST_STRNEQ(spc_tag_get_song_artist(tag), "Nob", 32);
	ST_EQ(spc_tag_get_default_channel_disables(tag), true);
	ST_EQ(spc_tag_get_emulator_used_to_dump(tag), 1);

	return "ok";
}

spc_tag_test tests[] = {
	{.name = "parse_file", .init = spc_tag_init, .function = spc_tag_parse_test,
		.cleanup = spc_tag_cleanup},
	{.name = "parse_memory", .init = spc_tag_init, .function = spc_tag_parse_memory_test,
		.cleanup = spc_tag_cleanup},
	{.name = "write", .init = spc_tag_init, .function = spc_tag_write_test,
		.cleanup = spc_tag_cleanup},
	{.name = "accessors", .init = spc_tag_init, .function =
		spc_tag_accessors_test, .cleanup = spc_tag_cleanup},
};

size_t count = sizeof(tests)/sizeof(tests[0]);

int main()
{
	for (size_t i = 0; i < count; ++i)
	{
		spc_tag_tests_register(&tests[i]);
	}

	int result = spc_tag_tests_run();
	spc_tag_tests_free();
	return result;
}
