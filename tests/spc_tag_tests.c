#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <spc_tag_tests.h>

static const char * spc_tag_test_run(spc_tag_test *test)
{
	test->init(&test->data);
	const char* result = test->function(test->data, test->description, sizeof(test->description));
	test->cleanup(test->data);

	if (strncmp("ok", result, 2) == 0)
		snprintf(test->description, sizeof(test->description), "PASS");

	return result;
}

static spc_tag_test *tests = NULL;
static size_t tests_size = 0;
static size_t tests_capacity = 0;

void spc_tag_tests_register(spc_tag_test *test)
{
	if (!tests)
	{
		tests = calloc(5, sizeof(*tests));
		tests_capacity = 5;
	}
	if (tests_size == tests_capacity)
	{
		spc_tag_test *new_tests = realloc(tests, tests_capacity*2*sizeof(*tests));
        if (new_tests)
            tests = new_tests;
        else
            abort();
		tests_capacity *= 2;
	}
	tests[tests_size++] = *test;
}

void spc_tag_tests_free(void)
{
	free(tests);
	tests_size = tests_capacity = 0;
}

int spc_tag_tests_run(void)
{
	const char *description_fmt = "1..%zu\n";
	const char *test_fmt = "%s %zu %s: %s\n";

	printf(description_fmt, tests_size);
	for (size_t i = 0; i < tests_size; ++i)
	{
		const char *result = spc_tag_test_run(&tests[i]);
		printf(test_fmt, result, i+1, tests[i].name, tests[i].description);
	}
	return 0;
}
