#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <spc_tag/spc_tag_xid666.h>
#include <spc_tag_tests.h>

#ifndef DATA_DIR
#error "Don't know where to get unit test data from! Define DATA_DIR to the directory with unit test data."
#endif

struct test_data
{
	spc_tag_xid666 *xid666;
	spc_tag_xid666 *written;
	FILE *bad1;
	FILE *bad2;
	FILE *text;
	FILE *binary;
	FILE *temp;
};

#define xstr(s) str(s)
#define str(s) #s

void spc_tag_xid666_init(void **test_data)
{
	*test_data = malloc(sizeof(struct test_data));
	struct test_data *tdata = *test_data;
	tdata->xid666 = spc_tag_xid666_new();
	tdata->written = spc_tag_xid666_new();
	tdata->bad1 = fopen(xstr(DATA_DIR) "/bad1.spc", "rb");
	tdata->bad2 = fopen(xstr(DATA_DIR) "/bad2.spc", "rb");
	tdata->text = fopen(xstr(DATA_DIR) "/text.spc", "rb");
	tdata->binary = fopen(xstr(DATA_DIR) "/binary.spc", "rb");
	tdata->temp = tmpfile();
}

void spc_tag_xid666_cleanup(void *test_data)
{
	struct test_data *data = test_data;
	spc_tag_xid666_free(data->xid666);
	spc_tag_xid666_free(data->written);
	fclose(data->bad1);
	fclose(data->bad2);
	fclose(data->text);
	fclose(data->binary);
	fclose(data->temp);
	free(test_data);
}


ST_TEST_DEFINE(xid666_add)
{
	struct test_data *tdata = test_data;
	spc_tag_xid666 *chunk = tdata->xid666;
	spc_tag_xid666_add_data(chunk, 6, 20, NULL);

	// Check the three normal types
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 1);
//	ST_EQ(spc_tag_xid666_get_capacity(chunk), 5);
	spc_tag_xid666_data *subchunk = spc_tag_xid666_get_data(chunk, 0);
	ST_EQ(spc_tag_xid666_data_get_id(subchunk), 6);
	ST_EQ(spc_tag_xid666_data_get_type(subchunk), 0);
	ST_EQ(spc_tag_xid666_data_get_header_data(subchunk), 20);
	ST_EQ(spc_tag_xid666_data_get_data(subchunk), NULL);

	spc_tag_xid666_add_data(chunk, 1, 9, (unsigned char*)"testing!");
	subchunk = spc_tag_xid666_get_data(chunk, 1);
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 2);

	ST_EQ(spc_tag_xid666_data_get_id(subchunk), 1);
	ST_EQ(spc_tag_xid666_data_get_type(subchunk), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(subchunk), 9);
	ST_EQ(strlen((char*)spc_tag_xid666_data_get_data(subchunk)), 8)
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(subchunk), "testing!", 8);

	spc_tag_xid666_add_data(chunk, 5, 4, (unsigned char*)"\x12\x23\x34\x45");
	subchunk = spc_tag_xid666_get_data(chunk, 2);
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 3);
	ST_EQ(spc_tag_xid666_data_get_id(subchunk), 5);
	ST_EQ(spc_tag_xid666_data_get_type(subchunk), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(subchunk), 4);
	ST_STRNEQ(
		(char*)spc_tag_xid666_data_get_data(subchunk), "\x12\x23\x34\x45", 4);

	// FIXME should we allow the same subchunk/id to be added multiple times?
	spc_tag_xid666_add_data(chunk, 5, 4, (unsigned char*)"\x12\x23\x34\x45");
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 4);
	//ST_EQ(spc_tag_xid666_get_capacity(chunk), 5);
	spc_tag_xid666_add_data(chunk, 5, 4, (unsigned char*)"\x12\x23\x34\x45");
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 5);
	//ST_EQ(spc_tag_xid666_get_capacity(chunk), 5);
	spc_tag_xid666_add_data(chunk, 5, 4, (unsigned char*)"\x12\x23\x34\x45");
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 6);
	//ST_EQ(spc_tag_xid666_get_capacity(chunk), 10);
	// Check for bug where we forgot to include 0x13 ID as a valid String
	// xid666 id
	spc_tag_xid666_add_data(chunk, 0x13, 7, (unsigned char*)"abcdef");
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 7);

	// Alright, now, let's be nasty and start trying to add invalid data
	size_t current_count = spc_tag_xid666_get_chunks(chunk);
	ST_EQ(spc_tag_xid666_add_data(
		chunk, 0, 9, (unsigned char*)"testing!"), NULL);
	ST_EQ(current_count, spc_tag_xid666_get_chunks(chunk));
	ST_EQ(spc_tag_xid666_add_data(
		chunk, 5, 9, (unsigned char*)"testing!"), NULL);
	ST_EQ(current_count, spc_tag_xid666_get_chunks(chunk));
	ST_EQ(spc_tag_xid666_add_data(chunk, 1, 9, NULL), NULL);
	ST_EQ(current_count, spc_tag_xid666_get_chunks(chunk));
	// Strings must be 4 to 256 (null included) bytes long
	ST_EQ(spc_tag_xid666_add_data(chunk, 1, 3, (unsigned char*)"hi"), NULL);
	ST_EQ(current_count, spc_tag_xid666_get_chunks(chunk));

	return "ok";
}

ST_TEST_DEFINE(xid666_update)
{
	struct test_data *tdata = test_data;
	spc_tag_xid666 *chunk = tdata->xid666;
	spc_tag_xid666_add_data(chunk, 6, 20, NULL);
	spc_tag_xid666_add_data(chunk, 1, 9, (unsigned char*)"testing!");
	spc_tag_xid666_add_data(chunk, 5, 4, (unsigned char*)"\x12\x23\x34\x45");
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 3);

	spc_tag_xid666_data *data = spc_tag_xid666_get_data(chunk, 0);
	spc_tag_xid666_data_update(data, 0x11, 25, NULL);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x11);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 25);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	data = spc_tag_xid666_get_data(chunk, 1);
	spc_tag_xid666_data_update(data, 2, 5, (unsigned char*)"hi34");
	ST_EQ(spc_tag_xid666_data_get_id(data), 2);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 5);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "hi", 2);

	data = spc_tag_xid666_get_data(chunk, 2);
	spc_tag_xid666_data_update(
		data, 0x30, 4, (unsigned char*)"\x00\x00\x00\x12");
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x30);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ(
		(char*)spc_tag_xid666_data_get_data(data), "\x00\x00\x00\x12", 4);

	// Try changing the parameter type
	spc_tag_xid666_data_update(data, 0x3, 4, (unsigned char*)"abc");
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x3);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "abc", 3);

	spc_tag_xid666_data_update(data, 0x11, 2, (unsigned char*)"abc");
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x11);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 2);
	// As this type 0 is header only, the data field should be NULL'd
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	spc_tag_xid666_data_update(data, 0x6, 1,  NULL);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x6);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 1);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	// Now, try to update using invalid combination of parameters
	ST_EQ(spc_tag_xid666_data_update(data, 0, 1,  NULL), NULL);
	ST_EQ(spc_tag_xid666_data_update(data, 1, 10,  NULL), NULL);
	ST_EQ(spc_tag_xid666_data_update(
		data, 5, 9, (unsigned char*)"testing!"), NULL);

	return "ok";
}

#define xstr(s) str(s)
#define str(s) #s

ST_TEST_DEFINE(xid666_parse)
{
	struct test_data *tdata = test_data;
	spc_tag_xid666 *chunk = tdata->xid666;
	ST_EQ(spc_tag_xid666_parse_file(tdata->bad1, chunk), SPC_TAG_PARSE_ERR);
	ST_EQ(spc_tag_xid666_parse_file(tdata->bad2, chunk), SPC_TAG_IO_ERR);
	ST_EQ(spc_tag_xid666_parse_file(NULL, chunk), SPC_TAG_IO_ERR);

	// I made a custom file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	ST_EQ(spc_tag_xid666_parse_file(tdata->text, chunk), SPC_TAG_SUCCESS);
	//ST_STRNEQ((char*)spc_tag_xid666_get_type(chunk), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(chunk), 248);
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 6);

	// First chunk is title, second is game, third is date song was dumped as
	// a integer 0xyyyymmdd, fourth is the copyright year, fifth is the loop
	// length, sixth is a long comments string
	spc_tag_xid666_data *data = spc_tag_xid666_get_data(chunk, 0);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 1);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 11);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Title", 10);

	data = spc_tag_xid666_get_data(chunk, 1);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 2);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 10);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Game", 9);

	data = spc_tag_xid666_get_data(chunk, 2);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 5);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	unsigned char *date = spc_tag_xid666_data_get_data(data);
	ST_EQ(date[2] | (date[3] << 8), 2020);
	ST_EQ(date[1], 6);
	ST_EQ(date[0], 7);

	data = spc_tag_xid666_get_data(chunk, 3);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x14);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	uint16_t year = (0x7 << 8) | 0xE4; // 2020 little endian
	ST_EQ(spc_tag_xid666_data_get_header_data(data), year);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	data = spc_tag_xid666_get_data(chunk, 4);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x31);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "\xFF\x00\x00\00", 4);

	data = spc_tag_xid666_get_data(chunk, 5);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x07);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 192);
	const char * raw_comment =
		"ABC1232132132132132154648adsa65s4d2a6sf54a6d51a5sd1as65d1as65d1as6d" \
		"16a8w1d6a5d1s6a51d6as81d6s51d6a51da6s5d1a6s51d6a5s1d6as51d65as1d65a" \
		"s1d6sa51d65as1d65asdFAKEFAKEFAKEFAKEFAKEFAKEKEFAKEFAKEasd";
	ST_MEMCMP((char*)spc_tag_xid666_data_get_data(data), raw_comment, 192);

	// Make sure to free resources before re-using the chunk memory space
	spc_tag_xid666_destructor(chunk);
	spc_tag_xid666_constructor(chunk);

	// Now check binary parsing, all data SHOULD be the same
	ST_EQ(spc_tag_xid666_parse_file(tdata->binary, chunk), SPC_TAG_SUCCESS);
	//ST_STRNEQ((char*)spc_tag_xid666_get_type(chunk), "xid6", 4);
	ST_EQ(spc_tag_xid666_get_size(chunk), 248);
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 6);

	// First chunk is title, second is game, third is date song was dumped as
	// an integer yyyymmdd, fourth is the copyright year, fifth is the loop
	// length, sixth is a long comments string
	data = spc_tag_xid666_get_data(chunk, 0);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 1);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 11);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Title", 10);

	data = spc_tag_xid666_get_data(chunk, 1);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 2);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 10);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Game", 9);

	data = spc_tag_xid666_get_data(chunk, 2);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 5);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	date = spc_tag_xid666_data_get_data(data);
	ST_EQ(date[2] | (date[3] << 8), 2020);

	data = spc_tag_xid666_get_data(chunk, 3);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x14);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	year = (0x7 << 8) | 0xE4; // 2020 little endian
	ST_EQ(spc_tag_xid666_data_get_header_data(data), year);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	data = spc_tag_xid666_get_data(chunk, 4);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x31);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "\xFF\x00\x00\00", 4);

	data = spc_tag_xid666_get_data(chunk, 5);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x07);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 192);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), raw_comment, 192);

	spc_tag_xid666_destructor(chunk);
	return "ok";
}

ST_TEST_DEFINE(xid666_accessors)
{
	struct test_data *tdata = test_data;
	spc_tag_xid666 *chunk = tdata->xid666;
	// I made a custom file with data I wrote in with a hex editor, so the test
	// values were derived by me manually. Crosscheck that we parsed what we
	// expected
	ST_EQ(spc_tag_xid666_parse_file(tdata->text, chunk), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 6);
	ST_EQ(spc_tag_xid666_get_chunks(chunk), 6);

	// First chunk is title, second is game, third is date song was dumped as
	// an integer yyyymmdd, fourth is the copyright year, fifth is the loop
	// length, sixth is a long comments string
	spc_tag_xid666_data *data = spc_tag_xid666_get_data(chunk, 0);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 1);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 11);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Title", 10);

	data = spc_tag_xid666_get_data(chunk, 1);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 2);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 10);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Game", 9);

	data = spc_tag_xid666_get_data(chunk, 2);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 5);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	unsigned char *date = spc_tag_xid666_data_get_data(data);
	ST_EQ(date[2] | (date[3] << 8), 2020);
	ST_EQ(date[1], 6);
	ST_EQ(date[0], 7);

	data = spc_tag_xid666_get_data(chunk, 3);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x14);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	uint16_t year = (0x7 << 8) | 0xE4; // 2020 little endian
	ST_EQ(spc_tag_xid666_data_get_header_data(data), year);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	data = spc_tag_xid666_get_data(chunk, 4);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x31);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "\xFF\x00\x00\00", 4);

	data = spc_tag_xid666_get_data(chunk, 5);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x07);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 192);
	const char * raw_comment =
		"ABC1232132132132132154648adsa65s4d2a6sf54a6d51a5sd1as65d1as65d1as6d" \
		"16a8w1d6a5d1s6a51d6as81d6s51d6a51da6s5d1a6s51d6a5s1d6as51d65as1d65a" \
		"s1d6sa51d65as1d65asdFAKEFAKEFAKEFAKEFAKEFAKEKEFAKEFAKEasd";
	ST_MEMCMP((char*)spc_tag_xid666_data_get_data(data), raw_comment, 192);

	data = spc_tag_xid666_get_data(chunk, 6);
	ST_EQ(data, NULL);
	spc_tag_xid666_destructor(chunk);
	return "ok";
}

ST_TEST_DEFINE(xid666_write)
{
	struct test_data *tdata = test_data;
	spc_tag_xid666 *xid666 = tdata->xid666;

	// Check with a binary SPC,  that we can read, write, and read it back in
	// again
	ST_EQ(spc_tag_xid666_parse_file(tdata->binary, xid666), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_xid666_write_file(tdata->temp, xid666), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_xid666_parse_file(tdata->temp, tdata->written), SPC_TAG_SUCCESS);

	//ST_ACCESSOR_STRNEQ(spc_tag_xid666, type, xid666, tdata->written, 4);
	ST_ACCESSOR_EQ(spc_tag_xid666, size, xid666, tdata->written);
	ST_ACCESSOR_EQ(spc_tag_xid666, chunks, xid666, tdata->written);

	// First chunk is title, second is game, third is date song was dumped as
	// an integer yyyymmdd, fourth is the copyright year, fifth is the loop
	// length, sixth is a long comments string
	spc_tag_xid666_data *data = spc_tag_xid666_get_data(tdata->written, 0);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 1);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 11);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Title", 10);

	data = spc_tag_xid666_get_data(tdata->written, 1);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 2);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 10);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Game", 9);

	data = spc_tag_xid666_get_data(tdata->written, 2);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 5);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	unsigned char *date = spc_tag_xid666_data_get_data(data);
	ST_EQ(date[2] | (date[3] << 8), 2020);
	ST_EQ(date[1], 6);
	ST_EQ(date[0], 7);

	data = spc_tag_xid666_get_data(tdata->written, 3);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x14);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	uint16_t year = (0x7 << 8) | 0xE4; // 2020 little endian
	ST_EQ(spc_tag_xid666_data_get_header_data(data), year);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	data = spc_tag_xid666_get_data(tdata->written, 4);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x31);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "\xFF\x00\x00\00", 4);

	data = spc_tag_xid666_get_data(tdata->written, 5);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x07);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 192);
	const char * raw_comment =
		"ABC1232132132132132154648adsa65s4d2a6sf54a6d51a5sd1as65d1as65d1as6d" \
		"16a8w1d6a5d1s6a51d6as81d6s51d6a51da6s5d1a6s51d6a5s1d6as51d65as1d65a" \
		"s1d6sa51d65as1d65asdFAKEFAKEFAKEFAKEFAKEFAKEKEFAKEFAKEasd";
	ST_MEMCMP((char*)spc_tag_xid666_data_get_data(data), raw_comment, 192);

	spc_tag_xid666_destructor(tdata->written);
	spc_tag_xid666_constructor(tdata->written);
	spc_tag_xid666_destructor(xid666);
	spc_tag_xid666_constructor(xid666);

	// Now check with a text SPC
	ST_EQ(spc_tag_xid666_parse_file(tdata->text, xid666), SPC_TAG_SUCCESS);

	ST_EQ(spc_tag_xid666_write_file(tdata->temp, xid666), SPC_TAG_SUCCESS);
	ST_EQ(spc_tag_xid666_parse_file(tdata->temp, tdata->written), SPC_TAG_SUCCESS);

	//ST_ACCESSOR_STRNEQ(spc_tag_xid666, type, xid666, tdata->written, 4);
	ST_ACCESSOR_EQ(spc_tag_xid666, size, xid666, tdata->written);
	ST_ACCESSOR_EQ(spc_tag_xid666, chunks, xid666, tdata->written);

	// First chunk is title, second is game, third is date song was dumped as
	// an integer yyyymmdd, fourth is the copyright year, fifth is the loop
	// length, sixth is a long comments string
	data = spc_tag_xid666_get_data(tdata->written, 0);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 1);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 11);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Title", 10);

	data = spc_tag_xid666_get_data(tdata->written, 1);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 2);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 10);
	ST_EQ(spc_tag_xid666_data_get_data(data) != NULL, true);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "Fake Game", 9);

	data = spc_tag_xid666_get_data(tdata->written, 2);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 5);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	date = spc_tag_xid666_data_get_data(data);
	ST_EQ(date[2] | (date[3] << 8), 2020);
	ST_EQ(date[1], 6);
	ST_EQ(date[0], 7);

	data = spc_tag_xid666_get_data(tdata->written, 3);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x14);
	ST_EQ(spc_tag_xid666_data_get_type(data), 0);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), year);
	ST_EQ(spc_tag_xid666_data_get_data(data), NULL);

	data = spc_tag_xid666_get_data(tdata->written, 4);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x31);
	ST_EQ(spc_tag_xid666_data_get_type(data), 4);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 4);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), "\xFF\x00\x00\00", 4);

	data = spc_tag_xid666_get_data(tdata->written, 5);
	ST_EQ(data != NULL, true);
	ST_EQ(spc_tag_xid666_data_get_id(data), 0x07);
	ST_EQ(spc_tag_xid666_data_get_type(data), 1);
	ST_EQ(spc_tag_xid666_data_get_header_data(data), 192);
	ST_STRNEQ((char*)spc_tag_xid666_data_get_data(data), raw_comment, 192);

	spc_tag_xid666_destructor(xid666);
	return "ok";
}


spc_tag_test tests[] = {
	{.name = "xid666_add", .init = spc_tag_xid666_init, .function =
		spc_tag_xid666_add_test, .cleanup = spc_tag_xid666_cleanup},
	{.name = "xid666_update", .init = spc_tag_xid666_init, .function =
		spc_tag_xid666_update_test, .cleanup = spc_tag_xid666_cleanup},
	{.name = "xid666_parse", .init = spc_tag_xid666_init, .function =
		spc_tag_xid666_parse_test, .cleanup = spc_tag_xid666_cleanup},
	{.name = "xid666_accessors", .init = spc_tag_xid666_init, .function =
		spc_tag_xid666_accessors_test, .cleanup = spc_tag_xid666_cleanup},
	{.name = "xid666_write", .init = spc_tag_xid666_init, .function =
		spc_tag_xid666_write_test, .cleanup = spc_tag_xid666_cleanup}
};

size_t count = sizeof(tests)/sizeof(tests[0]);

int main()
{
	for (size_t i = 0; i < count; ++i)
	{
		spc_tag_tests_register(&tests[i]);
	}

	int result = spc_tag_tests_run();
	spc_tag_tests_free();
	return result;
}
