#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <spc_tag/spc_tag_memory_accessor.h>
#include <spc_tag_tests.h>

#ifndef DATA_DIR
#error "Don't know where to get unit test data from! Define DATA_DIR to the directory with unit test data."
#endif

struct test_data
{
	spc_tag_memory memory;
	spc_tag_accessor accessor;
};

#define xstr(s) str(s)
#define str(s) #s

void spc_tag_memory_accessor_init_(void **test_data)
{
	*test_data = malloc(sizeof(struct test_data));
	struct test_data *tdata = *test_data;
	spc_tag_memory_constructor(&tdata->memory, 1000);
	for (size_t i = 0; i < 1000; ++i)
	{
		tdata->memory.data[i] = i;
	}
	spc_tag_memory_accessor_init(&tdata->accessor, &tdata->memory);
}

void spc_tag_memory_accessor_cleanup(void *test_data)
{
	struct test_data *tdata = test_data;
	spc_tag_memory_destructor(&tdata->memory);
	free(test_data);
}

ST_TEST_DEFINE(seek)
{
	struct test_data *tdata = test_data;
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 0, SEEK_CUR), 0);
	ST_EQ(tdata->memory.pos, 0u);
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 1000, SEEK_CUR), 0);
	ST_EQ(tdata->memory.pos, 1000u);
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 1, SEEK_CUR), 0);
	ST_EQ(tdata->memory.pos, 1001u);
	tdata->memory.pos = SIZE_MAX;
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 1000, SEEK_CUR), -1);
	tdata->memory.pos = 0;
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, -1, SEEK_CUR), -1);

	tdata->memory.pos = 0;
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 0, SEEK_SET), 0);
	ST_EQ(tdata->memory.pos, 0u);
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 1000, SEEK_SET), 0);
	ST_EQ(tdata->memory.pos, 1000u);
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, -1, SEEK_SET), -1);
	ST_EQ(tdata->memory.pos, 1000u);

	tdata->memory.pos = 0;
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, 0, SEEK_END), 0);
	ST_EQ(tdata->memory.pos, tdata->memory.size);
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, -1000, SEEK_END), 0);
	ST_EQ(tdata->memory.pos, 0);
	ST_EQ(spc_tag_accessor_seek(&tdata->accessor, -10000, SEEK_END), -1);
	ST_EQ(tdata->memory.pos, 0);
	return "ok";
}

ST_TEST_DEFINE(read)
{
	struct test_data *tdata = test_data;
	unsigned char buffer[1000] = {0};
	ST_EQ(spc_tag_accessor_read(&tdata->accessor, buffer, 1), 1);
	ST_EQ(buffer[0], 0u);
	ST_EQ(buffer[1], 0u);
	ST_EQ(spc_tag_accessor_read(&tdata->accessor, buffer + 1, 1), 1u);
	ST_EQ(buffer[0], 0u);
	ST_EQ(buffer[1], 1u);
	ST_EQ(spc_tag_accessor_read(&tdata->accessor, buffer + 2, 254), 254u);
	for (size_t i = 0; i < 256; ++i)
	{
		ST_EQ(buffer[i], i);
	}
	ST_EQ(buffer[256], 0u);
	ST_EQ(buffer[257], 0u);
	tdata->memory.pos = 0;
	ST_EQ(spc_tag_accessor_read(&tdata->accessor, buffer, 1000), 1000u);
	for (size_t i = 0; i < 1000; ++i)
	{
		ST_EQ(buffer[i], i % 256);
	}
	tdata->memory.pos = 0;
	ST_EQ(spc_tag_accessor_read(&tdata->accessor, buffer, 10000), 1000u);
	ST_EQ(spc_tag_accessor_read(&tdata->accessor, buffer, 10000), 0u);
	return "ok";
}

ST_TEST_DEFINE(write)
{
	struct test_data *tdata = test_data;
	unsigned char buffer[1000] = {0};
	for (size_t i = 0; i < 1000; ++i)
	{
		buffer[i] = 0xFC;
	}
	ST_EQ(spc_tag_accessor_write(&tdata->accessor, buffer, 1), 1);
	ST_EQ(tdata->memory.data[0], 0xFCu);
	ST_EQ(tdata->memory.data[1], 1u);
	ST_EQ(spc_tag_accessor_write(&tdata->accessor, buffer + 1, 1), 1u);
	ST_EQ(tdata->memory.data[0], 0xFCu);
	ST_EQ(tdata->memory.data[1], 0xFCu);
	ST_EQ(spc_tag_accessor_write(&tdata->accessor, buffer + 2, 254), 254u);
	for (size_t i = 0; i < 256; ++i)
	{
		ST_EQ(tdata->memory.data[i], 0xFCu);
	}
	ST_EQ(tdata->memory.data[256], 0u);
	ST_EQ(tdata->memory.data[257], 1u);
	tdata->memory.pos = 0;
	ST_EQ(spc_tag_accessor_write(&tdata->accessor, buffer, 1000), 1000u);
	for (size_t i = 0; i < 1000; ++i)
	{
		ST_EQ(tdata->memory.data[i], 0xFCu);
	}
	ST_EQ(spc_tag_accessor_write(&tdata->accessor, buffer, 1000), 1000u);
	for (size_t i = 0; i < 2000; ++i)
	{
		ST_EQ(tdata->memory.data[i], 0xFCu);
	}
	ST_EQ(tdata->memory.size, 2000u);
	ST_EQ(tdata->memory.capacity, 2000u);
	unsigned char foobar[10000];
	ST_EQ(spc_tag_accessor_write(&tdata->accessor, foobar, sizeof(foobar)), sizeof(foobar));
	ST_EQ(tdata->memory.size, 12000u);
	ST_EQ(tdata->memory.capacity, 16000u);
	return "ok";
}

spc_tag_test tests[] = {
	{.name = "seek", .init = spc_tag_memory_accessor_init_, .function = spc_tag_seek_test,
		.cleanup = spc_tag_memory_accessor_cleanup},
	{.name = "read", .init = spc_tag_memory_accessor_init_, .function = spc_tag_read_test,
		.cleanup = spc_tag_memory_accessor_cleanup},
	{.name = "write", .init = spc_tag_memory_accessor_init_, .function =
		spc_tag_write_test, .cleanup = spc_tag_memory_accessor_cleanup},
};

size_t count = sizeof(tests)/sizeof(tests[0]);

int main()
{
	for (size_t i = 0; i < count; ++i)
	{
		spc_tag_tests_register(&tests[i]);
	}

	int result = spc_tag_tests_run();
	spc_tag_tests_free();
	return result;
}
