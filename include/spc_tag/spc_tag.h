// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
/// @file
#ifndef SPC_TAG_H_
#define SPC_TAG_H_

#include <spc_tag/spc_tag_metadata.h>
#include <spc_tag/spc_tag_accessor.h>

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Represents the entirety of the contents of a SPC file.
 */
typedef struct spc_tag spc_tag;

/** Initialize the given spc_tag.
 *
 * This initializes the internal data structures of spc_tag. It must be
 * deinitialized using spc_tag_destructor before the memory holding the object
 * is released (either via the stack unrolling, or being freed).
 *
 * @param[in] data Pointer to struct to initialize.
 */
void spc_tag_constructor(spc_tag *data);

/** Deinitialize the given spc_tag.
 *
 * The pointer given isn't freed by this function, the function merely
 * deinitializes the internal structures, freeing any internally held
 * resources.
 *
 * @param[in] data Pointer to struct to initialize.
 */
void spc_tag_destructor(spc_tag *data);

/** Returns a pointer to an allocated spc_tag.
 *
 * Must be freed with spc_tag_free.
 *
 * @returns A pointer to a new spc_tag struct.
 */
spc_tag *spc_tag_new(void);

/** Frees resources used by the SPC data struct.
 *
 * @param[in] data data struct to free.
 *
 * @pre metadata must have been allocated by the spc_tag_data_new function.
 * @post All resources held by the SPC data struct are freed, and the given
 * pointer is also freed.
 */
void spc_tag_free(spc_tag *data);

/** Parses the entirety of the SPC file.
 *
 * @param[in,out] file Open file handle to the SPC file to read.
 * @param[out] data Pointer to spc_tag to use for storing the results. The
 *                  results must be freed by using spc_tag_free (xid666
 *                  requires the allocation of data on the heap).
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 * @post The file location position has been moved to the end of the file, or
 *  to the end of the xid666 block, whichever comes first.
 */
spc_tag_error spc_tag_parse_file(FILE *file, spc_tag *data);

/** Parses the entirety of the SPC file provided as a memory buffer.
 *
 * @param[in] memory Memory buffer holding the SPC data.
 * @param[in] size The size in bytes of the memory buffer.
 * @param[out] data Pointer to spc_tag to use for storing the results. The
 *                  results must be freed by using spc_tag_free (xid666
 *                  requires the allocation of data on the heap).
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 */
spc_tag_error spc_tag_parse_memory(
	unsigned char *memory, size_t size, spc_tag *data);

/** Parses the entirety of the SPC file.
 *
 * @param[in,out] accessor Generic accessor providing access to some IO
 *                         resource. It must support seeking and reading.
 * @param[out] data Pointer to spc_tag to use for storing the results. The
 *                  results must be freed by using spc_tag_free.
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 * @post The accessor location position has been moved to the end of the file,
 *  or to the end of the xid666 block, whichever comes first.
 */
spc_tag_error spc_tag_parse(spc_tag_accessor *accessor, spc_tag *data);

/** Write full SPC file and metadata given the spc_tag passed in.
 *
 * @param[in,out] file File to write to. It must be opened in some writable
 *                     mode.
 * @param[in] data Data to save out to disk.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure. The most
 *  common failure is being unable to write to the file for any reason (out of
 *  space, wrong mode, etc.)
 *
 * @post The file's position is set to the end of the last xid666 subchunk, or
 *  the end of the xi666 header if there are no xid666 subchunks.
 */
spc_tag_error spc_tag_write_file(FILE *file, const spc_tag *data);

/** Write full SPC file and metadata given the spc_tag passed in.
 *
 * @param[in,out] accessor Generic accessor providing access to some IO
 *                         resource. It must support seeking and writing.
 * @param[in] data Data to save out to disk.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 *
 * @post The accessor's position is set to the end of the last xid666 subchunk,
 *  or the end of the xi666 header if there are no xid666 subchunks.
 */
spc_tag_error spc_tag_write(spc_tag_accessor *accessor, const spc_tag *data);

#define DECLARE_SPC(TYPE, NAME) \
TYPE spc_tag_get_##NAME(const spc_tag *data); \
spc_tag_error spc_tag_set_##NAME(spc_tag *data, TYPE value);

#define DECLARE_SPC_ARRAY(TYPE, NAME) \
TYPE spc_tag_get_##NAME(const spc_tag *data); \
spc_tag_error spc_tag_set_##NAME(spc_tag *data, TYPE value, size_t length);

/**
 * @anchor Accessors
 * @name Accessors
 * The following functions are accessors for spc_tag. These are provided to
 * facilitate the use of this library with other languages, as it is not
 * feasible for them to know the exact struct size and alignment.
 *
 * The accessors have a signature of:
 *
 * ```
 * TYPE spc_tag_get_NAME(const spc_tag *data);
 * bool spc_tag_set_NAME(spc_tag *data, TYPE value);
 * ```
 *
 * And for array types:
 *
 * ```
 * TYPE spc_tag_get_NAME(const spc_tag *data);
 * bool spc_tag_set_NAME(spc_tag *data, TYPE value, size_t length);
 * ```
 *
 * For array types, these are the maximum lengths supported by the library (not
 * including the null terminator):
 *  - spc_tag.ram: 0x10000
 *  - spc_tag.dsp_regs: 128
 *  - spc_tag.extra_ram: 64
 *  - spc_tag_id666.song_title: 32
 *  - spc_tag_id666.game_title: 32
 *  - spc_tag_id666.name_of_dumper: 16
 *  - spc_tag_id666.comments: 32
 *  - spc_tag_id666.song_artist: 32
 *
 * The library makes sure that all of the strings are null terminated (like
 * song_title). This happens even if all 32 bytes of spc_tag_id666.song_title,
 * for example, set by the @ref spc_tag_set_song_title function. The set array
 * functions will fail if given a length longer than what they support. Binary
 * arrays, like spc_tag.ram, spc_tag.dsp_regs, and spc_tag.extra_ram, do not do
 * any such preprocessing, merely copy the number of bytes specified (so long
 * as the amount specified is less than or equal to the full length of the
 * field).
 *
 * @tparam TYPE is the type of the underlying data (see the spc_tag,
 *              spc_tag_metadata, spc_tag_id666 and spc_tag_xid666 structs for
 *              details)
 * @tparam NAME The name of the field being accessed (see the spc_tag,
 *              spc_tag_metadata, spc_tag_id666 and spc_tag_xid666 structs for
 *              details)
 *
 * @param[in,out] data Data to read from in get functions, data saved to in
 *                set functions.
 * @param[in] value Data to copy into the metadata.
 * @param[in] length Length of the array to copy into the metadata.
 * @returns The value of the requested parameter for get, for set it returns
 *  true on success, false on failure, usually meaning the length parameter was
 *  too big for what the specific field supports.
 */

/** Getter/Accessor for spc_tag_metadata. See @ref Accessors for details.
 */
///@{
DECLARE_SPC(uint16_t, pc)
DECLARE_SPC(uint8_t, a)
DECLARE_SPC(uint8_t, x)
DECLARE_SPC(uint8_t, y)
DECLARE_SPC(uint8_t, psw)
DECLARE_SPC(uint8_t, sp)
DECLARE_SPC(uint16_t, reserved)
DECLARE_SPC_ARRAY(const unsigned char *, ram)
DECLARE_SPC_ARRAY(const unsigned char *, dsp_regs)
DECLARE_SPC_ARRAY(const unsigned char *, extra_ram)
DECLARE_SPC(bool, has_id666)
DECLARE_SPC(uint8_t, minor_version)
DECLARE_SPC(bool, binary)
DECLARE_SPC_ARRAY(const char *, song_title)
DECLARE_SPC_ARRAY(const char *, game_title)
DECLARE_SPC_ARRAY(const char *, name_of_dumper)
DECLARE_SPC_ARRAY(const char *, comments)
DECLARE_SPC(struct tm, dump_date)
DECLARE_SPC(unsigned, seconds_until_fade)
DECLARE_SPC(unsigned, fade_length_ms)
DECLARE_SPC_ARRAY(const char *, song_artist)
DECLARE_SPC(bool, default_channel_disables)
DECLARE_SPC(uint8_t, emulator_used_to_dump)
DECLARE_SPC(bool, has_xid666)
///@}
#undef DECLARE_SPC
#undef DECLARE_SPC_ARRAY

/** Get a pointer to the xid666 chunk.
 *
 * @param[in] data SPC data structure to query for xid666 chunk.
 *
 * @returns A pointer to the xid666 chunk.
 */
spc_tag_xid666 *spc_tag_get_xid666(spc_tag *data);

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_H_
