// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
/// @file
#ifndef SPC_TAG_XID666_H_
#define SPC_TAG_XID666_H_

#include <spc_tag/spc_tag_error.h>
#include <spc_tag/spc_tag_accessor.h>

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Represents a single xid666 subchunk of a SPC file.
 *
 * @anchor id_table
 * These are all of the valid subchunks, copied from the specification:
 *  ID   | Type    | Size  | Description
 * ------|---------|-------|------------------------------------------------------------
 *  0x01 | String  | 4-256 | Song name
 *  0x02 | String  | 4-256 | Game name
 *  0x03 | String  | 4-256 | Artist's name
 *  0x04 | String  | 4-256 | Dumper's name
 *  0x05 | Integer | 4     | Date song was dumped (stored as little endian 0xyyyymmdd)
 *  0x06 | Data    | 1     | Emulator used
 *  0x07 | String  | 4-256 | Comments
 *  0x10 | String  | 4-256 | Official Soundtrack Title
 *  0x11 | Data    | 1     | OST disc
 *  0x12 | Data    | 2     | OST track (upper byte is the number 0-99, lower byte is an optional ASCII character)
 *  0x13 | String  | 4-256 | Publisher's name
 *  0x14 | Data    | 2     | Copyright year
 *  0x30 | Integer | 4     | Introduction length (lengths are stored in 1/64000th seconds)
 *  0x31 | Integer | 4     | Loop length
 *  0x32 | Integer | 4     | End length
 *  0x33 | Integer | 4     | Fade length
 *  0x34 | Data    | 1     | Muted voices (a bit is set for each voice that's muted)
 *  0x35 | Data    | 1     | Number of times to loop
 *  0x36 | Data    | 1     | Mixing (preamp) level
 *
 * @anchor type_table
 * Valid types:
 *  Value | Type    | Description
 * -------|---------|----------------------------------------------------------
 *  0x00  | Data    | Data is saved in the 'data' item of the sub-chunk header, in little-endian format
 *  0x01  | String  | Data is stored as a null terminated string (max 256 characters including null). Strings are saved using 8-bit character encoding. Support for unicode may be added in the future.
 *  0x04  | Integer | Data is stored as a 32-bit little-endian integer
 */
typedef struct spc_tag_xid666_data spc_tag_xid666_data;

/** Represents the xid666 chunk of a SPC file.
 */
typedef struct spc_tag_xid666 spc_tag_xid666;

/** Allocate and construct a new xid666 chunk.
 *
 * @returns A pointer to the newly allocated and initialized xid666 chunk.
 */
spc_tag_xid666 *spc_tag_xid666_new(void);

/** Deiniitalize and free the memory used by the given xid666 chunk.
 *
 * @param[in,out] chunk Chunk to free.
 *
 * @post Chunk is freed.
 */
void spc_tag_xid666_free(spc_tag_xid666 *chunk);

/** Prepares xid666 chunk for use.
 *
 * @param[out] chunk Chunk to initialize.
 *
 * @post Chunk is initialized and ready for use. It has no subchunks.
 */
void spc_tag_xid666_constructor(spc_tag_xid666 *chunk);

/** Frees resources used by the xid666 chunk.
 *
 * @param[in,out] chunk Chunk to free.
 *
 * @post All resources held by the xid666 chunk are freed.
 */
void spc_tag_xid666_destructor(spc_tag_xid666 *chunk);

/** Parses the xid666 chunk from the given file.
 *
 * @param[in,out] file File to parse xid666 chunk from. File position must be
 *                     set to the beginning of the xid666 chunk header.
 * @param[out] chunk Storage for the parsed spc_tag_xid666. This must be
 *                   cleaned up by using spc_tag_xid666_destructor.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 * @post The file location position has been moved to just past the last
 *       subchunk.
 */
spc_tag_error spc_tag_xid666_parse_file(FILE *file, spc_tag_xid666 *chunk);

/** Parses the xid666 chunk from the given accessor.
 *
 * @param[in,out] accessor Accessor to parse xid666 chunk from. File position
 *                         must be set to the beginning of the xid666 chunk
 *                         header. Accessor must support seeking and reading.
 * @param[out] chunk Storage for the parsed spc_tag_xid666. This must be
 *                   cleaned up by using spc_tag_xid666_destructor.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 * @post The file location position has been moved to just past the last
 *       subchunk.
 */
spc_tag_error spc_tag_xid666_parse(spc_tag_accessor *accessor, spc_tag_xid666 *chunk);

/** Adds a new xid666 subchunk to the given spc_tag_xid666 based on the
 * contents of the file.
 *
 * @param[in,out] file File to parse xid666 subchunk data from. File position
 *                     must be set to the beginning of a valid subchunk.
 * @param[in,out] chunk spc_tag_xid666 to hold the newly-parsed subchunk.
 *
 * @returns A pointer to the new subchunk.
 * @post The file location position has been moved to just past the subchunk
 *       parsed.
 */
spc_tag_xid666_data* spc_tag_xid666_data_parse_file(
	FILE *file, spc_tag_xid666 *chunk);

/** Adds a new xid666 subchunk to the given spc_tag_xid666 based on the
 * contents of the accessor.
 *
 * @param[in,out] accessor Accessor to parse xid666 subchunk data from. File
 *  position must be set to the beginning of a valid subchunk. Accessor must
 *  support seeking and reading.
 * @param[in,out] chunk spc_tag_xid666 to hold the newly-parsed subchunk.
 *
 * @returns A pointer to the new subchunk.
 * @post The file location position has been moved to just past the subchunk
 *       parsed.
 */
spc_tag_xid666_data* spc_tag_xid666_data_parse(
	spc_tag_accessor *accessor, spc_tag_xid666 *chunk);

/** Writes out the xid666 chunk to the given file.
 *
 * @param[in,out] file File to write xid666 chunk data to.
 * @param[in] data Data to write.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 *
 * @post File position will be left at the end of the last subchunk written.
 */
spc_tag_error spc_tag_xid666_write_file(FILE *file, const spc_tag_xid666 *data);

/** Writes out the xid666 chunk to the given accessor.
 *
 * @param[in,out] accessor Accessor to write xid666 chunk data to. Accessor
 *                         must support seeking and writing.
 * @param[in] data Data to write.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 *
 * @post File position will be left at the end of the last subchunk written.
 */
spc_tag_error spc_tag_xid666_write(spc_tag_accessor *accessor, const spc_tag_xid666 *data);

/** Writes out an xid666 subchunk to the given file.
 *
 * @param[in,out] file File to write xid666 subchunk data to. File position
 *                     must be aligned to a 4 byte boundary, after the xid666
 *                     header or after the end of another xid666 subchunk.
 * @param[in] data Data to write.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 */
spc_tag_error spc_tag_xid666_data_write_file(FILE *file, const spc_tag_xid666_data *data);

/** Writes out an xid666 subchunk to the given accessor.
 *
 * @param[in,out] accessor Accessor to write xid666 subchunk data to. File position
 *                         must be aligned to a 4 byte boundary, after the
 *                         xid666 header or after the end of another xid666
 *                         subchunk. Accessor must support seeking and writing.
 * @param[in] data Data to write.
 *
 * @returns SPC_TAG_SUCCESS on success, something else on failure.
 */
spc_tag_error spc_tag_xid666_data_write(spc_tag_accessor *accessor, const spc_tag_xid666_data *data);

/** Add a new xid666 subchunkto the xid666 chunk using the given data.
 *
 * @param[in,out] chunk Chunk to add subchunk to.
 * @param[in] id ID of new subchunk. The type field is automatically determined
 *               based on the ID.
 * @param[in] header_data Either the size of the data element of a subchunk
 *                        (types 1 or 4), or the actual data (type 0)
 * @param[in] data Actual data to copy into the subchunk IF the type is 1 or 4.
 *
 * @returns A pointer to the new subchunk.
 * @post Subchunk has been added to the back of the given chunk. Size field of
 *  xid666 is updated to reflect new addition.
 */
spc_tag_xid666_data* spc_tag_xid666_add_data(
	spc_tag_xid666* chunk,
	uint8_t id,
	uint16_t header_data,
	const unsigned char *data);

/** Modifies an existing xid666 subchunk using the given data.
 *
 * @param[in,out] subchunk Subchunk to modify.
 * @param[in] id New ID of subchunk. The type field is automatically determined
 *               based on the ID.
 * @param[in] header_data Either the size of the new data element of a subchunk
 *                        (types 1 or 4), or the actual data (type 0).
 * @param[in] data Actual data to copy into the subchunk IF the type is 1 or 4.
 *
 * @returns A pointer to the modified subchunk, should be the same as the
 *  subchunk input variable.
 */
spc_tag_xid666_data* spc_tag_xid666_data_update(
	spc_tag_xid666_data* subchunk,
	uint8_t id,
	uint16_t header_data,
	const unsigned char* data);

/** Returns a pointer to the specified chunk.
 *
 * @param[in] xid666 XID666 chunk to search in.
 * @param[in] index Index of subchunk to find.
 *
 * @returns A pointer to the specified subchunk if it exists, NULL otherwise.
 */
spc_tag_xid666_data *spc_tag_xid666_get_data(spc_tag_xid666 *xid666, size_t index);

/** Get the ID field of the given xid666 subchunk.
 *
 * @param[in] data Subchunk to query.
 *
 * @returns The ID of the given subchunk.
 * @see The @ref id_table "ID table" for details on valid ID and their
 *  associated types.
 * @see The @ref type_table "type table" for details on types.
 */
uint8_t spc_tag_xid666_data_get_id(const spc_tag_xid666_data *data);

/** Get the type field of the given xid666 subchunk.
 *
 * There is a direct mapping between the ID and the type, specified in the SPC
 * file format. The format does specify that there is a type field in the
 * subchunk.
 *
 * @param[in] data Subchunk to query.
 *
 * @returns The type of the given subchunk.
 * @see The @ref id_table "ID table" for details on valid ID and their
 *  associated types.
 * @see The @ref type_table "type table" for details on types.
 */
uint8_t spc_tag_xid666_data_get_type(const spc_tag_xid666_data *data);

/** Get the data size field, or the actual data (depending on type)  of the
 * given xid666 subchunk.
 *
 * @param[in] data Subchunk to query.
 *
 * @returns The the size field for String and Integer types, or the actual data
 * for type Data, of the given subchunk.
 * @see The @ref id_table "ID table" for details on valid ID and their
 *  associated types.
 * @see The @ref type_table "type table" for details on types.
 */
uint16_t spc_tag_xid666_data_get_header_data(const spc_tag_xid666_data *data);

/** Get the pointer to the data field of the given xid666 subchunk.
 *
 * @param[in] data Subchunk to query.
 *
 * @returns The pointer to the data field of the given xid666 subchunk. It is
 * NULL for the Data type, and should be a buffer of size header_data for the
 * other two types.
 *
 * @see The @ref id_table "ID table" for details on valid ID and their
 *  associated types.
 * @see The @ref type_table "type table" for details on types.
 */
unsigned char* spc_tag_xid666_data_get_data(const spc_tag_xid666_data *data);

/** Get the size in bytes of all of the subchunks.
 *
 * @param[in] xid666 Chunk to query.
 *
 * @returns The size in bytes of all of the subchunks.
 */
size_t spc_tag_xid666_get_size(const spc_tag_xid666 *xid666);

/** Get the number of subchunks in the given chunk.
 *
 * @param[in] xid666 Chunk to query.
 *
 * @returns The number of subchunks in the givne chunk.
 */
size_t spc_tag_xid666_get_chunks(const spc_tag_xid666 *xid666);

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_XID666_H_
