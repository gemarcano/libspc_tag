// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2021
/// @file
#ifndef SPC_TAG_MEMORY_ACCESSOR_H_
#define SPC_TAG_MEMORY_ACCESSOR_H_

#include <spc_tag/spc_tag_accessor.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Type of a memory buffer to be used by an accessor.
 *
 * This contains all the information required to have a memory buffer act like
 * a FILE so that memory accessors can implement the required functions for the
 * interface.
 */
typedef struct spc_tag_memory
{
	/** Pointer to the memory buffer. */
	unsigned char *data;
	/** Size of the data stored. Always less than or equal to the capacity. */
	size_t size;
	/** Capacity of the memory buffer. */
	size_t capacity;
	/** Current position of the memory "stream". */
	size_t pos;
} spc_tag_memory;

/** Construct an spc_tag_memory object.
 *
 * Allocates an internal buffer of the given size. Internal position is set to
 * 0.
 *
 * @param[in,out] memory Memory to initialize.
 * @param[in] size Initial size of the buffer to allocate. The accessor will
 *  grow the buffer if it needs to be larger.
 */
void spc_tag_memory_constructor(spc_tag_memory *memory, size_t size);

/** Destroy an spc_tag_memory object.
 *
 * Frees all resources grabbed by the constructor.
 *
 * @param[in,out] memory Memory to free.
 */
void spc_tag_memory_destructor(spc_tag_memory *memory);

/** Initialize an accessor to be used as a wrapper around a memory buffer.
 *
 * @param[out] accessor Pointer to the accessor to initialize.
 * @param[in,out] memory Pointer to memory descriptor to associate with
 *  accessor.
 *
 * @post The accessor has been initialized and can be used with other
 *  spc_tag_accessor function calls to read, write, and seek.
 */
void spc_tag_memory_accessor_init(
	spc_tag_accessor *accessor, spc_tag_memory *memory);

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_MEMORY_ACCESSOR_H_
