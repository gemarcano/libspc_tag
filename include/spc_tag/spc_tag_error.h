// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
/// @file
#ifndef SPC_TAG_ERROR_H_
#define SPC_TAG_ERROR_H_

#ifdef __cplusplus
extern "C" {
#endif

/// Represents an error returned by an spc_tag_* function
typedef enum spc_tag_error
{
	SPC_TAG_SUCCESS,
	SPC_TAG_IO_ERR,
	SPC_TAG_PARSE_ERR
} spc_tag_error;

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_ERROR_H_
