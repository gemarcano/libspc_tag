// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
/// @file
#ifndef SPC_TAG_ACCESSOR_H_
#define SPC_TAG_ACCESSOR_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Type of an accessor.
 *
 * Accessors exist to provide a generic way to implement IO, to allow reading
 * data from any source that can be made to behave similarly to a file. It is
 * up to the implementation of specific accessors whether they will support the
 * full interface or only some of the functions. Any function not supported
 * will have its pointer in the accessor set to NULL.
 */
typedef struct spc_tag_accesssor
{
	/** Pointer to an accessor read function.
	 *
	 * These functions should work similarly to fread(data, X, 1, file), where X is
	 * the amount of data to read, except this function returns the number of bytes
	 * read.
	 *
	 * @param[in, out] data Generic pointer to some data used by the function.
	 * @param[out] dest Destination buffer.
	 * @param[in] amount Amount of bytes to read into the destination buffer.
	 *
	 * @return The number of bytes read. If it's different than the amount
	 *  requested, an error took place.
	 * @post The position of the IO accessor is updated to amount bytes forward
	 *  on success.
	 */
	size_t (*read)(void *data, void *dest, size_t amount);

	/** Pointer to an accessor write function.
	 *
	 * These functions should work similarly to fwrite(src, X, 1, file), where X is
	 * the amount of data to write, except this function returns the number of
	 * bytes written.
	 *
	 * @param[in, out] data Generic pointer to some data used by the function.
	 * @param[in] src Source buffer.
	 * @param[in] amount Amount of bytes to write from the source buffer.
	 *
	 * @return The number of bytes written. If it's different than the amount
	 *  requested, an error took place.
	 * @post The position of the IO accessor is updated to amount bytes forward
	 *  on success.
	 */
	size_t (*write)(void *data, const void *src, size_t amount);

	/** Pointer to an accessor seek function.
	 *
	 * These functions should work similarly to fseek(file, pos, flag), where pos
	 * is the position to set the IO to, and flag is one of SEEK_CUR, SEEK_SET, or
	 * SEEK_END. The flags affect the seeking behavior identically to how fseek
	 * behaves.
	 *
	 * @param[in, out] data Generic pointer to some data used by the function.
	 * @param[in] position Position to set the buffer to.
	 * @param[in] flag Determine from where the position is calculated (beginning,
	 *  current, end).
	 *
	 * @return 0 on success, -1 on failure.
	 */
	int (*seek)(void *data, long position, int flag);

	/** Data to pass to the accessor functions, accessor implementation defined
	 *  as to what it is.
	 */
	void *data;
} spc_tag_accessor;

/** Call the read function of the accessor.
 *
 * @param[in,out] accessor Accessor to read data from.
 * @param[out] dest Destination buffer.
 * @param[in] amount Amount of data to read.
 *
 * @return The number of bytes read. If the number is less than the amount
 *  requested, an error took place.
 * @post The position of the IO accessor is updated to amount bytes forward
 *  on success.
 */
size_t spc_tag_accessor_read(
	spc_tag_accessor *accessor, void *dest, size_t amount);

/** Call the write function of the accessor.
 *
 * @param[in,out] accessor Accessor to write data to.
 * @param[in] src Source buffer to write data from.
 * @param[in] amount Amount of data to write from the source buffer.
 *
 * @return The number of bytes written. If the number is less than the amount
 *  requested, an error took place.
 * @post The position of the IO accessor is updated to amount bytes forward
 *  on success.
 */
size_t spc_tag_accessor_write(
	spc_tag_accessor *accessor, const void *src, size_t amount);

/** Call the seek function of the accessor.
 *
 * @param[in,out] accessor Accessor to set the IO position to the specified
 *                         location.
 * @param[in] pos Position to set the buffer to.
 * @param[in] flag Determine from where the position is calculated (SEEK_SET,
 *                 SEEK_CUR, SEEK_END). See fseek documentation for more
 *                 information.
 *
 * @return 0 on success, -1 on failure.
 */
int spc_tag_accessor_seek(spc_tag_accessor *accessor, long pos, int flag);

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_ACCESSOR_H_
