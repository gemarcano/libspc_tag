// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
/// @file
#ifndef SPC_TAG_FILE_ACCESSOR_H_
#define SPC_TAG_FILE_ACCESSOR_H_

#include <spc_tag/spc_tag_accessor.h>

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Initialize an accessor to be used as a wrapper around a FILE.
 *
 * @param[out] accessor Pointer to the accessor to initialize.
 * @param[in,out] data File pointer to associate with the accessor.
 *
 * @post The accessor has been initialized and can be used with other
 *  spc_tag_accessor function calls to read, write, and seek.
 */
void spc_tag_file_accessor_init(spc_tag_accessor *accessor, FILE *data);

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_FILE_ACCESSOR_H_
