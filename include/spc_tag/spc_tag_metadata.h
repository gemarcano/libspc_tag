// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
/// @file
#ifndef SPC_TAG_METADATA_H_
#define SPC_TAG_METADATA_H_

#include <spc_tag/spc_tag_xid666.h>
#include <spc_tag/spc_tag_accessor.h>

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <time.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Represents only the metadata/id666 of a SPC file.
 *
 * All strings have a padding byte, meant to be '\0', as the SPC documentation
 * does not specify if the strings are null terminated.
 */
typedef struct spc_tag_metadata spc_tag_metadata;

/** Initialize the given spc_tag_metadata.
 *
 * This initializes the internal data structures of spc_tag_metadata. It must
 * be deinitialized using spc_tag_metadata_destructor before the memory holding
 * the object is released (either via the stack unrolling, or being freed).
 *
 * @param[in] data Pointer to struct to initialize.
 */
void spc_tag_metadata_constructor(spc_tag_metadata *data);

/** Deinitialize the given spc_tag_metadata.
 *
 * The pointer given isn't freed by this function, the function merely
 * deinitializes the internal structures, freeing any internally held
 * resources.
 *
 * @param[in] metadata Pointer to struct to initialize.
 */
void spc_tag_metadata_destructor(spc_tag_metadata *metadata);

/** Returns a pointer to an allocated spc_tag_metadata.
 *
 * Must be freed with spc_tag_metadata_free.
 *
 * @returns A pointer to a new spc_tag_metadata.
 */
spc_tag_metadata *spc_tag_metadata_new(void);

/** Frees resources used by the SPC metadata struct.
 *
 * @param[in] metadata metadata struct to free.
 *
 * @pre metadata must have been allocated by the spc_tag_metadata_new function.
 * @post All resources held by the SPC metadata struct are freed, and the given
 * pointer is also freed.
 */
void spc_tag_metadata_free(spc_tag_metadata *metadata);

/** Parses the id666 and xid666 fields only of the SPC file.
 *
 * @param[in,out] file Open file handle to the SPC file to read.
 * @param[out] metadata Pointer to spc_tag_metadata to use for storing the
 *                      results. The results must be freed by using
 *                      spc_tag_metadata_free.
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 * @post The file location position has been moved to the end of the file, or
 *       to the end of the xid666 block, whichever comes first.
 */
spc_tag_error spc_tag_metadata_parse_file(
	FILE *file, spc_tag_metadata *metadata);

/** Parses the id666 and xid666 fields only of the SPC file held in a memory
 * buffer.
 *
 * @param[in] memory Memory buffer holding SPC data to parse.
 * @param[in] size Size of memory buffer.
 * @param[out] metadata Pointer to spc_tag_metadata to use for storing the
 *                      results. The results must be freed by using
 *                      spc_tag_metadata_free.
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 * @post The file location position has been moved to the end of the file, or
 *       to the end of the xid666 block, whichever comes first.
 */
spc_tag_error spc_tag_metadata_parse_memory(
	unsigned char *memory, size_t size, spc_tag_metadata *metadata);

/** Parses the id666 and xid666 fields only from the given accessor.
 *
 * @param[in,out] accessor Accessor to read from. It must support read and seek
 *                         functionality.
 * @param[out] metadata Pointer to spc_tag_metadata to use for storing the
 *                      results. The results must be freed by using
 *                      spc_tag_metadata_free.
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 * @post The accessor location position has been moved to the end of the file,
 *  or to the end of the xid666 block, whichever comes first.
 */
spc_tag_error spc_tag_metadata_parse(
	spc_tag_accessor *accessor, spc_tag_metadata *metadata);

/** Writes the id666 and xid666 metadata to the given file from the given
 *  spc_tag_metadata.
 *
 * This function writes out the SPC file header, but does not fill in any
 * emulation parameters (register, RAM, etc.).
 *
 * @param[in,out] file File to write to. It must be opened in some writable
 *                     mode.
 * @param[in] metadata Metadata to save out to disk.
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure. The most
 *  common failure is being unable to write to the file for any reason (out of
 *  space, wrong mode, etc.).
 * @post The position location of the file is advanced to the end of the file,
 *  past all data written, on success.
 */
spc_tag_error spc_tag_metadata_write_file(
	FILE *file, const spc_tag_metadata *metadata);

/** Writes the id666 and xid666 metadata to the given accessor from the given
 *  spc_tag_metadata.
 *
 * This function writes out the SPC file header, but does not fill in any
 * emulation parameters (register, RAM, etc.).
 *
 * @param[in,out] accessor Accessor to write to. It must support write and seek
 *                         functionality.
 * @param[in] metadata Metadata to save out to disk.
 *
 * @returns SPC_TAG_SUCCESS on success, anything else on failure.
 * @post The position location of the accessor is advanced to the end of the
 *  file, past all data written, on success.
 */
spc_tag_error spc_tag_metadata_write(
	spc_tag_accessor *accessor, const spc_tag_metadata *metadata);

#define DECLARE_METADATA_SPC(TYPE, NAME) \
TYPE spc_tag_metadata_get_##NAME(const spc_tag_metadata *metadata); \
spc_tag_error spc_tag_metadata_set_##NAME(spc_tag_metadata *metadata, TYPE value);

#define DECLARE_METADATA_SPC_ARRAY(TYPE, NAME) \
TYPE spc_tag_metadata_get_##NAME(const spc_tag_metadata *metadata); \
spc_tag_error spc_tag_metadata_set_##NAME(spc_tag_metadata *metadata, TYPE value, size_t length);

/**
 * @anchor maccessors
 * @name Accessors
 * The following functions are accessors for spc_tag_metadata. These are
 * provided to facilitate the use of this library with other languages, as it
 * is not feasible for them to know the exact struct size and alignment.
 *
 * The accessors have a signature of:
 *
 * ```
 * TYPE spc_tag_metadata_get_NAME(const spc_tag_metadata *metadata);
 * bool spc_tag_metdata_set_NAME(spc_tag_metadata *metadata, TYPE value);
 * ```
 *
 * And for array types:
 *
 * ```
 * TYPE spc_tag_metadata_get_NAME(const spc_tag_metadata *metadata);
 * bool spc_tag_metdata_set_NAME(spc_tag_metadata *metadata, TYPE value, size_t length);
 * ```
 *
 * For array types, these are the maximum lengths supported by the library (not
 * including the null terminator):
 *  - spc_tag_id666.song_title: 32
 *  - spc_tag_id666.game_title: 32
 *  - spc_tag_id666.name_of_dumper: 16
 *  - spc_tag_id666.comments: 32
 *  - spc_tag_id666.song_artist: 32
 *
 * The library makes sure that all of the strings are null terminated when
 * returned from an accessor, even if all 32 bytes of spc_tag_id666.song_title,
 * for example, set by the @ref spc_tag_metadata_set_song_title function. The
 * set array functions will fail if given a length longer than what they
 * support.
 *
 * @tparam TYPE is the type of the underlying data (see the spc_tag_metadata,
 *              spc_tag_id666 and spc_tag_xid666 structs for details)
 * @tparam NAME The name of the field being accessed (see the spc_tag_metadata,
 *              spc_tag_id666 and spc_tag_xid666 structs for details)
 *
 * @param[in,out] metadata Data to read from in get functions, data saved to in
 *                set functions.
 * @param[in] value Data to copy into the metadata.
 * @param[in] length Length of the array to copy into the metadata.
 * @returns The value of the requested parameter for get, for set it returns
 *  true on success, false on failure, usually meaning the length parameter was
 *  too big for what the specific field supports.
 */

/** Getter/Accessor for spc_tag_metadata. See @ref maccessors "Accessors" for
 * details.
 */
///@{
DECLARE_METADATA_SPC(bool, has_id666)
DECLARE_METADATA_SPC(uint8_t, minor_version)
DECLARE_METADATA_SPC(bool, binary)
DECLARE_METADATA_SPC_ARRAY(const char *, song_title)
DECLARE_METADATA_SPC_ARRAY(const char *, game_title)
DECLARE_METADATA_SPC_ARRAY(const char *, name_of_dumper)
DECLARE_METADATA_SPC_ARRAY(const char *, comments)
DECLARE_METADATA_SPC(struct tm, dump_date)
DECLARE_METADATA_SPC(unsigned, seconds_until_fade)
DECLARE_METADATA_SPC(unsigned, fade_length_ms)
DECLARE_METADATA_SPC_ARRAY(const char *, song_artist)
DECLARE_METADATA_SPC(bool, default_channel_disables)
DECLARE_METADATA_SPC(uint8_t, emulator_used_to_dump)
DECLARE_METADATA_SPC(bool, has_xid666)
///@}
#undef DECLARE_METADATA_SPC_ARRAY
#undef DECLARE_METADATA_SPC

/** Get a pointer to the xid666 structure.
 *
 * @param[in] metadata Metadata to query for xid666 structure.
 *
 * @returns A pointer to the xid666 structure.
 */
spc_tag_xid666 *spc_tag_metadata_get_xid666(spc_tag_metadata *metadata);

#ifdef __cplusplus
}
#endif

#endif//SPC_TAG_METADATA_H_
