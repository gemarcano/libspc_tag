// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <spc_tag/spc_tag_metadata.h>
#include <spc_tag/spc_tag_file_accessor.h>
#include <spc_tag/spc_tag_memory_accessor.h>
#include <spc_tag/spc_tag_accessor.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

struct spc_tag_metadata
{
	/** Whether the SPC has ID666 data or not */
	bool has_id666;
	/** The minor version of the SPC file */
	uint8_t minor_version;
	/** Whether ID666 metadata is in binary or text format */
	bool binary;
	/** Title of the song.
	 *
	 * This field is meant to hold a maximum of 32 bytes of data, leaving the
	 * final 33rd byte to be a mandatory null character in case the 32 other
	 * bytes aren't null terminated.
	 */
	char song_title[33];
	/** Title of the game.
	 *
	 * This field is meant to hold a maximum of 32 bytes of data, leaving the
	 * final 33rd byte to be a mandatory null character in case the 32 other
	 * bytes aren't null terminated.
	 */
	char game_title[33];
	/** Name of the person that dumped this SPC.
	 *
	 * This field is meant to hold a maximum of 16 bytes of data, leaving the
	 * final 33rd byte to be a mandatory null character in case the 32 other
	 * bytes aren't null terminated.
	 */
	char name_of_dumper[17];
	/** Comments.
	 *
	 * This field is meant to hold a maximum of 32 bytes of data, leaving the
	 * final 33rd byte to be a mandatory null character in case the 32 other
	 * bytes aren't null terminated.
	 */
	char comments[33];
	/** Date when the SPC was dumped.
	 *
	 * This field only uses the year, month, and day fields of the struct tm.
	 */
	struct tm dump_date;
	/** Seconds until fade. */
	unsigned seconds_until_fade;
	/** Length of fade effect. */
	unsigned fade_length_ms;
	/** Song artist.
	 *
	 * This field is meant to hold a maximum of 32 bytes of data, leaving the
	 * final 33rd byte to be a mandatory null character in case the 32 other
	 * bytes aren't null terminated.
	 */
	char song_artist[33];
	/** Default channel disables
	 *
	 * Honestly, I don't know what this is supposed to be, but it's in the
	 * specification. It is true if the default channles should be disabled, I
	 * suppose.
	 * */
	bool default_channel_disables;
	/** Numeric representation of which emulator was used to dump this SPC.
	 *
	 * Known emulators:
	 *  0 - Unknown
	 *  1 - ZSNES
	 *  2 - Snes9x
	 */
	uint8_t emulator_used_to_dump;

	/** Whether the SPC files has xid666 data */
	bool has_xid666;

	/** XID666 data */
	spc_tag_xid666 *xid666;
};

// Check that an SPC file is in binary format
static bool check_binary(spc_tag_accessor *file)
{
	spc_tag_accessor_seek(file, 0xD2, SEEK_SET);
	char null;
	if (spc_tag_accessor_read(file, &null, 1) != 1)
	{
		// This is non-sensical, but there's no real way to report a failure
		// here
		return true;
	}
	return null == '\x00';
}

// Check the very beginning of the file to make sure it's an SPC file.
static spc_tag_error check_beginning(spc_tag_accessor *accessor)
{
	// Reset pointer now that we know
	spc_tag_accessor_seek(accessor, 0, SEEK_SET);
	char head_string[33];
	if (spc_tag_accessor_read(accessor, head_string, 33) != 33)
		return SPC_TAG_IO_ERR;
	if (strncmp(head_string, "SNES-SPC700 Sound File Data v", 29))
		return SPC_TAG_PARSE_ERR;
	uint16_t fixed_bytes;
	if (spc_tag_accessor_read(accessor, &fixed_bytes, 2) != 2)
		return SPC_TAG_IO_ERR;
	if (fixed_bytes != 0x1a1a)
		return SPC_TAG_PARSE_ERR;

	return SPC_TAG_SUCCESS;
}

uint16_t bcd_to_u16(uint16_t bcd)
{
	uint16_t result = 0;
	for (size_t i = 0; i < 4; ++i)
	{
		unsigned ten = 1;
		for (size_t j = 0; j < i; ++j)
			ten *= 10;
		result += ((bcd >> (i*4))& 0xF) * ten;
	}
	return result;
}

uint8_t bcd_to_u8(uint8_t bcd)
{
	uint8_t result = 0;
	for (size_t i = 0; i < 2; ++i)
	{
		unsigned ten = 1;
		for (size_t j = 0; j < i; ++j)
			ten *= 10;
		result += ((bcd >> (i*4))& 0xF) * ten;
	}
	return result;
}

static spc_tag_error parse_metadata(spc_tag_accessor *accessor, spc_tag_metadata *metadata)
{
	metadata->binary = check_binary(accessor);
	spc_tag_accessor_seek(accessor, 0x23, SEEK_SET);
	unsigned char has_id666;
	if (spc_tag_accessor_read(accessor, &has_id666, 1) != 1)
		return SPC_TAG_IO_ERR;
	metadata->has_id666 = has_id666 == 0x1a;
	// Textual minor version doesn't have room for more than 2 characters
	// FIXME should we check that this matches what's in the text header? We might need to return something from check beginning
	if (spc_tag_accessor_read(accessor, &metadata->minor_version, 1) != 1)
		return SPC_TAG_IO_ERR;
	if (metadata->minor_version > 99)
		return SPC_TAG_PARSE_ERR;

	if (metadata->has_id666)
	{
		spc_tag_accessor_seek(accessor, 0x2E, SEEK_SET);
		if (spc_tag_accessor_read(accessor, &metadata->song_title, 32) != 32 ||
			spc_tag_accessor_read(accessor, &metadata->game_title, 32) != 32 ||
			spc_tag_accessor_read(accessor, &metadata->name_of_dumper, 16) != 16 ||
			spc_tag_accessor_read(accessor, &metadata->comments, 32) != 32)
		{
			return SPC_TAG_IO_ERR;
		}

		if (metadata->binary)
		{
			// Date is in little endian on disk, first byte is day, second is
			// month, last 2 are year
			unsigned char raw[4];
			if (spc_tag_accessor_read(accessor, &raw, 4) != 4)
				return SPC_TAG_IO_ERR;
			if (memcmp(&raw, "\x00\x00\x00\x00", 4))
			{
				metadata->dump_date.tm_year = (raw[2] | (raw[3] << 8)) - 1900;
				metadata->dump_date.tm_mon = raw[1] - 1;
				metadata->dump_date.tm_mday = raw[0];
			}

			// Jump to number of seconds until fade, skipping unused space
			spc_tag_accessor_seek(accessor, 0xA9, SEEK_SET);

			// time is little endian on disk.
			if (spc_tag_accessor_read(accessor, &raw, 3) != 3)
				return SPC_TAG_IO_ERR;
			metadata->seconds_until_fade = raw[0] | raw[1] << 8 | raw[2] << 16;
			if (spc_tag_accessor_read(accessor, &raw, 4) != 4)
				return SPC_TAG_IO_ERR;
			metadata->fade_length_ms =
				raw[0] | raw[1] << 8 | raw[2] << 6 | raw[3] << 24;

			if (spc_tag_accessor_read(accessor, metadata->song_artist, 32) != 32)
				return SPC_TAG_IO_ERR;

			unsigned char ch;
			if (spc_tag_accessor_read(accessor, &ch, 1) != 1)
				return SPC_TAG_IO_ERR;
			metadata->default_channel_disables = ch == 1;
			if (spc_tag_accessor_read(accessor, &ch, 1) != 1)
				return SPC_TAG_IO_ERR;
			metadata->emulator_used_to_dump = ch;
		}
		else
		{
			char date[12];
			date[11] = '\0';
			if (spc_tag_accessor_read(accessor, date, 11) != 11)
				return SPC_TAG_IO_ERR;

			if (*date)
			{
				metadata->dump_date.tm_mon = atoi(date) - 1;
				metadata->dump_date.tm_mday = atoi(date + 3);
				metadata->dump_date.tm_year = atoi(date + 6) - 1900;
			}

			spc_tag_accessor_seek(accessor, 0xA9, SEEK_SET);

			char time_buffer[6] = {0};
			if (spc_tag_accessor_read(accessor, time_buffer, 3) != 3)
				return SPC_TAG_IO_ERR;
			// FIXME this only checks if the first byte is non-zero! We should check if the string is a valid number
			if (*time_buffer)
				metadata->seconds_until_fade = atoi(time_buffer);
			if (spc_tag_accessor_read(accessor, time_buffer, 5) != 5)
				return SPC_TAG_IO_ERR;
			// FIXME this only checks if the first byte is non-zero! We should check if the string is a valid number
			if (*time_buffer)
				metadata->fade_length_ms = atoi(time_buffer);
			if (spc_tag_accessor_read(accessor, metadata->song_artist, 32) != 32)
				return SPC_TAG_IO_ERR;

			char ch;
			if (spc_tag_accessor_read(accessor, &ch, 1) != 1)
				return SPC_TAG_IO_ERR;
			metadata->default_channel_disables = ch == '1';
			if (spc_tag_accessor_read(accessor, &ch, 1) != 1)
				return SPC_TAG_IO_ERR;
			metadata->emulator_used_to_dump = ch - '0';
		}
	}

	spc_tag_error result = SPC_TAG_SUCCESS;
	// xid666
	char xid666[4]; // Just a test buffer, not going to check it
	if (!spc_tag_accessor_seek(accessor, 0x10200, SEEK_SET) && spc_tag_accessor_read(accessor, xid666, 4))
	{
		metadata->has_xid666 = true;
		result = spc_tag_xid666_parse(accessor, metadata->xid666);
	}
	return result;
}

void spc_tag_metadata_constructor(spc_tag_metadata *data)
{
	if (!data)
		return;

	*data = (spc_tag_metadata){0};
	data->xid666 = spc_tag_xid666_new();
}

void spc_tag_metadata_destructor(spc_tag_metadata *metadata)
{
	if (!metadata)
		return;

	spc_tag_xid666_free(metadata->xid666);
	metadata->xid666 = NULL;
}

spc_tag_metadata *spc_tag_metadata_new(void)
{
	spc_tag_metadata *data = malloc(sizeof(*data));
	spc_tag_metadata_constructor(data);
	return data;
}

void spc_tag_metadata_free(spc_tag_metadata *metadata)
{
	if (metadata == NULL)
		return;

	spc_tag_metadata_destructor(metadata);
	free(metadata);
}

spc_tag_error spc_tag_metadata_parse_file(FILE *file, spc_tag_metadata *metadata)
{
	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);

	if (!file)
		return SPC_TAG_IO_ERR;

	return spc_tag_metadata_parse(&accessor, metadata);
}

spc_tag_error spc_tag_metadata_parse_memory(unsigned char *memory, size_t size,  spc_tag_metadata *metadata)
{
	spc_tag_accessor accessor;
	if (!memory)
		return SPC_TAG_IO_ERR;

	spc_tag_memory mem;
	spc_tag_memory_constructor(&mem, size);
	memcpy(mem.data, memory, size);
	spc_tag_memory_accessor_init(&accessor, &mem);

	return spc_tag_metadata_parse(&accessor, metadata);
}

spc_tag_error spc_tag_metadata_parse(spc_tag_accessor *accessor, spc_tag_metadata *metadata)
{
	if (!accessor)
		return SPC_TAG_IO_ERR;

	spc_tag_error result = check_beginning(accessor);
	if (result != SPC_TAG_SUCCESS)
		return result;

	return parse_metadata(accessor, metadata);
}

spc_tag_error spc_tag_metadata_write_file(
	FILE *file, const spc_tag_metadata* metadata)
{
	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_metadata_write(&accessor, metadata);
}

spc_tag_error spc_tag_metadata_write(
	spc_tag_accessor *accessor, const spc_tag_metadata* metadata)
{
	// Minor version must be 2 digits, and greater than 30
	if (30 > metadata->minor_version || metadata->minor_version > 99)
		return SPC_TAG_PARSE_ERR;

	spc_tag_accessor_seek(accessor, 0x0, SEEK_SET);

	// Write out basic header information
	char header[37];
	snprintf(header, sizeof(header), "SNES-SPC700 Sound File Data v0.%02hhu\x1a\x1a",
		metadata->minor_version);

	spc_tag_accessor_write(accessor, header, 35);
	spc_tag_accessor_write(accessor, metadata->has_id666 ? "\x1a" : "\x1b", 1);
	spc_tag_accessor_write(accessor, &metadata->minor_version, 1);

	// Skip over registers, we're only writing out the metadata
	spc_tag_accessor_seek(accessor, 0x2E, SEEK_SET);

	// If we don't have tag data, just bail out
	if (!metadata->has_id666)
	{
		spc_tag_accessor_seek(accessor, 0x10200, SEEK_SET);
		if (spc_tag_accessor_write(accessor, "xid6", 4) != 4)
			return SPC_TAG_IO_ERR;

		spc_tag_accessor_seek(accessor, 0x10208, SEEK_SET);
		return SPC_TAG_SUCCESS;
	}

	if (spc_tag_accessor_write(accessor, &metadata->song_title, 32) != 32 ||
		spc_tag_accessor_write(accessor, &metadata->game_title, 32) != 32 ||
		spc_tag_accessor_write(accessor, &metadata->name_of_dumper, 16) != 16 ||
		spc_tag_accessor_write(accessor, &metadata->comments, 32) != 32)
	{
		return SPC_TAG_IO_ERR;
	}

	// days: [0,31], months: [0,11]. years:[0,9999]
	// We have 4 characters available for years
	// day == 0 just means there's no date
	const struct tm *dump_date = &metadata->dump_date;
	if ((dump_date->tm_mon < 0 || dump_date->tm_mon > 11) ||
		(dump_date->tm_mday < 0 || dump_date->tm_mday > 31) ||
		(dump_date->tm_year < -1900 || dump_date->tm_year > (9999 - 1900)))
	{
		return SPC_TAG_PARSE_ERR;
	}

	if (metadata->binary)
	{
		// Only write out if there's data to write out
		if (dump_date->tm_mday)
		{
			char date_raw[4];
			uint16_t year = dump_date->tm_year + 1900;
			date_raw[2] = year;
			date_raw[3] = year >> 8;
			date_raw[1] = dump_date->tm_mon + 1;
			date_raw[0] = dump_date->tm_mday;
			if (spc_tag_accessor_write(accessor, &date_raw, 4) != 4)
				return SPC_TAG_IO_ERR;
		}

		// Skip over unused bytes
		spc_tag_accessor_seek(accessor, 0xA9, SEEK_SET);
		char time_buffer[7];
		// in theory seconds_until_fade and fade_length_ms should already be
		// clamped to 3 bytes and 4 bytes, respectively
		time_buffer[2] = metadata->seconds_until_fade >> 16;
		time_buffer[1] = metadata->seconds_until_fade >> 8;
		time_buffer[0] = metadata->seconds_until_fade;

		time_buffer[6] = metadata->fade_length_ms >> 24;
		time_buffer[5] = metadata->fade_length_ms >> 16;
		time_buffer[4] = metadata->fade_length_ms >> 8;
		time_buffer[3] = metadata->fade_length_ms;
		const char *disables = metadata->default_channel_disables ?
			"\x01" : "\x00";
		if (spc_tag_accessor_write(accessor, time_buffer, 7) != 7 ||
			spc_tag_accessor_write(accessor, &metadata->song_artist, 32) != 32 ||
			spc_tag_accessor_write(accessor, disables, 1) != 1 ||
			spc_tag_accessor_write(accessor, &metadata->emulator_used_to_dump, 1) != 1)
		{
			return SPC_TAG_IO_ERR;
		}
	}
	else // textual data format
	{

		if (dump_date->tm_mday)
		{
			char date[12];
			snprintf(date, 12, "%02u/%02u/%04u",
				(dump_date->tm_mon + 1),
				dump_date->tm_mday,
				dump_date->tm_year + 1900);
			if (spc_tag_accessor_write(accessor, date, 11) != 11)
				return SPC_TAG_IO_ERR;
		}
		else
		{
			// Make sure to skip/zero date data
			spc_tag_accessor_seek(accessor, 11, SEEK_CUR);
		}

		// Since the seconds and ms until fade could be greater than what the
		// text fields allow (binary format allows for larger numbers), clamp
		// the numbers if necessary to the maximum the text fields allow
		unsigned seconds_until_fade = metadata->seconds_until_fade;
		if (seconds_until_fade > 999)
			seconds_until_fade = 999;
		unsigned fade_length_ms = metadata->fade_length_ms;
		if (fade_length_ms > 99999)
			fade_length_ms = 99999;
		char time_buffer[9];
		snprintf(time_buffer, 9, "%03u%05u",
			seconds_until_fade,
			fade_length_ms);

		unsigned char disable =
			metadata->default_channel_disables ? '1' : '0';
		char emulator[2];
		snprintf(emulator, 2, "%hhu", metadata->emulator_used_to_dump);
		if (spc_tag_accessor_write(accessor, time_buffer, 8) != 8 ||
			spc_tag_accessor_write(accessor, metadata->song_artist, 32) != 32 ||
			spc_tag_accessor_write(accessor, &disable, 1) != 1 ||
			spc_tag_accessor_write(accessor, emulator, 1) != 1)
		{
			return SPC_TAG_IO_ERR;
		}
	}

	spc_tag_error result = SPC_TAG_SUCCESS;
	if (metadata->has_xid666)
		result = spc_tag_xid666_write(accessor, metadata->xid666);

	return result;
}

#define DEFINE_META_GET(TYPE, NAME) \
TYPE spc_tag_metadata_get_ ## NAME(const spc_tag_metadata *metadata) \
{ \
	return metadata->NAME; \
}

#define DEFINE_META_GETSET(TYPE, NAME) \
DEFINE_META_GET(TYPE, NAME) \
spc_tag_error spc_tag_metadata_set_ ## NAME(spc_tag_metadata *metadata, TYPE value) \
{ \
	metadata->NAME = value; \
	return SPC_TAG_SUCCESS; \
}

#define DEFINE_META_GETSET_ARRAY(TYPE, NAME) \
TYPE spc_tag_metadata_get_ ## NAME(const spc_tag_metadata *metadata) \
{ \
	return metadata->NAME; \
} \
spc_tag_error spc_tag_metadata_set_ ## NAME( \
	spc_tag_metadata *metadata, TYPE value, size_t length) \
{ \
	static_assert(sizeof(metadata->NAME) > sizeof(void*), \
		"metadata fields appears to be a pointer and not an array!"); \
	if (length > sizeof(metadata->NAME) - 1) \
		return SPC_TAG_PARSE_ERR; \
	memcpy(metadata->NAME, value, length); \
	memset(metadata->NAME+length, 0, sizeof(metadata->NAME)); \
	return SPC_TAG_SUCCESS; \
}

DEFINE_META_GETSET(bool, has_id666)
DEFINE_META_GET(uint8_t, minor_version)
spc_tag_error spc_tag_metadata_set_minor_version(
	spc_tag_metadata *metadata, uint8_t value)
{
	// Don't accept a value > 99, as the textual minor_version in the file only
	// has two ASCII/UTF-8 character fields for the value
	if (value > 99)
		return SPC_TAG_PARSE_ERR;
	metadata->minor_version = value;
	return SPC_TAG_SUCCESS;
}
DEFINE_META_GETSET(bool, binary)
DEFINE_META_GETSET_ARRAY(const char *, song_title)
DEFINE_META_GETSET_ARRAY(const char *, game_title)
DEFINE_META_GETSET_ARRAY(const char *, name_of_dumper)
DEFINE_META_GETSET_ARRAY(const char *, comments)
DEFINE_META_GET(struct tm, dump_date)
spc_tag_error spc_tag_metadata_set_dump_date(
	spc_tag_metadata *metadata, struct tm value)
{
	// Do basic validation on the date. Note, this doesn't 100% catch
	// everything, like weird February dates or leap years or the like
	// We limit the year to 4 digits because that's the maximum the text form
	// can handle
	if ((value.tm_mon < 0 || value.tm_mon > 11) ||
		(value.tm_mday < 0 || value.tm_mday > 31) ||
		(value.tm_year < 0 || value.tm_year > 9999))
	{
		return SPC_TAG_PARSE_ERR;
	}
	metadata->dump_date = value;
	return SPC_TAG_SUCCESS;
}
DEFINE_META_GET(unsigned, seconds_until_fade)
spc_tag_error spc_tag_metadata_set_seconds_until_fade(
	spc_tag_metadata *metadata, unsigned value)
{
	// FIXME should we limit maximum depending on the id666 mode (text or
	// binary)?
	// For now, clamp to binary limits, 3 byte maximum
	if (value > 0xFFFFFF)
		return SPC_TAG_PARSE_ERR;
	metadata->seconds_until_fade = value;
	return SPC_TAG_SUCCESS;
}
DEFINE_META_GET(unsigned, fade_length_ms)
spc_tag_error spc_tag_metadata_set_fade_length_ms(
	spc_tag_metadata *metadata, unsigned value)
{
	// FIXME should we limit maximum depending on the id666 mode (text or
	// binary)?
	// For now, clamp to binary limits, 4 byte maximum
	if (value > 0xFFFFFFFF)
		return SPC_TAG_PARSE_ERR;
	metadata->fade_length_ms = value;
	return SPC_TAG_SUCCESS;
}
DEFINE_META_GETSET_ARRAY(const char *, song_artist)
DEFINE_META_GETSET(bool, default_channel_disables)
DEFINE_META_GETSET(uint8_t, emulator_used_to_dump)
DEFINE_META_GETSET(bool, has_xid666)

spc_tag_xid666 *spc_tag_metadata_get_xid666(spc_tag_metadata *metadata)
{
	return metadata->xid666;
}
