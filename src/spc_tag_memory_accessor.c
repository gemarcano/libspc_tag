// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2021

#include <spc_tag/spc_tag_accessor.h>
#include <spc_tag/spc_tag_memory_accessor.h>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

void spc_tag_memory_constructor(spc_tag_memory *memory, size_t size)
{
	memory->data = malloc(size);
	memory->size = size;
	memory->capacity = size;
	memory->pos = 0;
}

void spc_tag_memory_destructor(spc_tag_memory *memory)
{
	free(memory->data);
	memory->data = NULL;
	memory->pos = 0;
}

static size_t spc_tag_memory_read(void *data, void *dest, size_t amount)
{
	struct spc_tag_memory *mem = data;
	if (mem->pos == mem->size)
		return 0;
	if (amount + mem->pos < mem->pos) //wrap around
		amount = mem->size - mem->pos;
	else if (amount + mem->pos > mem->size)
		amount = mem->size - mem->pos;
	memcpy(dest, mem->data + mem->pos, amount);
	mem->pos += amount;
	return amount;
}

static size_t spc_tag_memory_write(void *data, const void *src, size_t amount)
{
	struct spc_tag_memory *mem = data;
	if (mem->pos == SIZE_MAX)
		return -1;
	if (amount + mem->pos < mem->pos) //wrap around
		amount = SIZE_MAX - mem->pos;
	while (mem->pos + amount > mem->capacity)
	{
		mem->data = realloc(mem->data, mem->capacity * 2);
		if (!mem->data)
			return 0;
		mem->capacity *= 2;
	}
	memcpy(mem->data + mem->pos, src, amount);
	mem->pos += amount;
	if (mem->pos > mem->size)
		mem->size = mem->pos;
	return amount;
}

static int spc_tag_memory_seek(void *data, long pos, int flag)
{
	struct spc_tag_memory *mem = data;
	size_t offset = (size_t)pos;
	switch (flag)
	{
		case SEEK_CUR:
			if (pos < 0 && -pos > mem->pos)
				return -1;
			offset += mem->pos;
			if (pos > 0 && offset < mem->pos)
				return -1;
			break;
		case SEEK_SET:
			if (pos < 0)
				return -1;
			break;
		case SEEK_END:
			if (pos < 0 && -pos > mem->size)
				return -1;
			offset += mem->size;
			break;
	}
	mem->pos = offset;
	return 0;
}

void spc_tag_memory_accessor_init(spc_tag_accessor *accessor, spc_tag_memory *memory)
{
	accessor->read = spc_tag_memory_read;
	accessor->write = spc_tag_memory_write;
	accessor->seek = spc_tag_memory_seek;
	accessor->data = memory;
}
