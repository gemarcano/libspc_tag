#include <spc_tag/spc_tag_accessor.h>
#include <spc_tag/spc_tag_file_accessor.h>

static size_t spc_tag_file_read(void *data, void *dest, size_t amount)
{
	return fread(dest, amount, 1, data) * amount;
}

static size_t spc_tag_file_write(void *data, const void *src, size_t amount)
{
	return fwrite(src, amount, 1, data) * amount;
}

static int spc_tag_file_seek(void *data, long pos, int flag)
{
	return fseek((FILE*)data, pos, flag);
}

void spc_tag_file_accessor_init(spc_tag_accessor *accessor, FILE *data)
{
	accessor->read = spc_tag_file_read;
	accessor->write = spc_tag_file_write;
	accessor->seek = spc_tag_file_seek;
	accessor->data = data;
}
