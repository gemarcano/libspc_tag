// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <spc_tag/spc_tag_xid666.h>
#include <spc_tag/spc_tag_error.h>
#include <spc_tag/spc_tag_file_accessor.h>
#include <spc_tag/spc_tag_accessor.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

struct spc_tag_xid666_data
{
	/** ID of XID666 subchunk. */
	uint8_t id;
	/** Type of the XID666 subchunk.
	 *
	 * Must be 0x00, 0x01, or 0x04
	 */
	uint8_t type;
	/** Data or size of data (depends on type).
	 *
	 * When the type is 0x00, this field contains the data, and the
	 * spc_tag_xid666_data.data should be NULL. If the type is 0x01 or 0x04,
	 * this field holds the size of the contents of the data pointed to by
	 * spc_tag_xid666_data.data.
	 */
	uint16_t header_data;
	/** Data, if the type requests that it exists. Its size is given by the
	 * spc_tag_xid666_data.header_data field.
	 */
	unsigned char *data;
};

struct spc_tag_xid666
{
	/** Holds the string "xid6" for a valid xid666 chunk.
	 *
	 * This string is null terminated.
	 */
	char type[5];
	/** The size of all subchunks combined in this chunk */
	uint32_t size;
	/** The number of subchunks found. */
	size_t nchunks;

	///@cond PRIVATE
	size_t capacity;
	spc_tag_xid666_data *data;
	///@endcond
};

// Check that an SPC file is in binary format
// Returns true only if the ID is one specified in the specification
static bool is_id_valid(uint8_t id)
{
	return (1 <= id && id <= 7) || (0x10 <= id && id <= 0x14) ||
		(0x30 <= id && id <= 0x36);
}

// Returns true only if the ID is valid in the specification
static bool is_type_valid(uint8_t type)
{
	return type == 0 || type == 1 || type == 4;
}

// Check that the given type and header_data combination are consitent
static bool sanity_check_type(uint8_t type, uint16_t header_data)
{
	if (!is_type_valid(type))
		return false;

	// Check string type size is reasonable
	if (type == 1 && !(4 <= header_data && header_data <= 256))
		return false;

	// Check int type size is reasonable
	if (type == 4 && (header_data != 4))
		return false;
	return true;
}

// Make sure other parameters make sense given the type and data.
// Also makes sure for String type that the data is null terminated.
static bool sanity_check_type_data(
	uint8_t type, uint16_t header_data, const unsigned char *data)
{
	// Check that type is consistent
	// That if the type has data, the data is not NULL, else data is NULL
	// And that if type is String, it's null terminated
	return sanity_check_type(type, header_data) &&
		((!type == !data) || !!data) &&
		(type != 0x01 || !data[header_data-1]);
}

// Returns the type given the ID, based on the specification, as all IDs have a
// type associated with them
static uint8_t get_type_from_id(uint8_t id)
{
	if ((id == 6) || (0x11 <= id && id <= 0x12) ||
			(0x14 == id) || (0x34 <= id && id <= 0x36))
		return 0;

	if ((1 <= id && id <= 4) || (id == 7) || (id == 0x10) || (id == 0x13))
		return 1;

	if ((5 == id) || (0x30 <= id && id <= 0x33))
		return 4;

	return 255;
}

// Returns the size of the subchunk data (excluding 4 byte header), with
// padding
static unsigned actual_subchunk_size(uint16_t header_data)
{
	return ((header_data + 3) / 4) * 4;
}

// Returns the number of bytes of padding for a subchunk with data
static unsigned subchunk_padding(uint16_t header_data)
{
	return actual_subchunk_size(header_data) - header_data;
}

// Implement adding an xid666_data subchunk to the xid666 chunk. See the
// spc_tag_xid666_add_data documentation for more information. The extra
// parameter, copy, indicates whether or not to copy the data parameter. If
// false, we assume data was allocated by malloc and we assume ownership over
// it. It's annoying, but we don't free the "moved" data if we fail.
static spc_tag_xid666_data* spc_tag_xid666_add_data_(
	spc_tag_xid666* chunk,
	uint8_t id,
	uint16_t header_data,
	const unsigned char* data,
	bool copy)
{
	if (!is_id_valid(id))
		return NULL;

	uint8_t type = get_type_from_id(id);
	if (!sanity_check_type_data(type, header_data, data))
		return NULL;

	if (chunk->nchunks == chunk->capacity)
	{
		size_t new_size = chunk->capacity * 2 * sizeof(*chunk->data);
		spc_tag_xid666_data *new_array = realloc(chunk->data, new_size);
		if (!new_array)
			return NULL;
		chunk->data = new_array;
		chunk->capacity *= 2;
	}
	spc_tag_xid666_data *result = &chunk->data[chunk->nchunks++];

	result->id = id;
	result->type = type;
	result->header_data = header_data;
	if (type)
	{
		if (copy)
		{
			result->data = malloc(header_data);
			memcpy(result->data, data, header_data);
		}
		else
		{
			// This is meant to handle the case where we're moving something
			// we've alloc'd using malloc, so we control the memory, so this
			// cast should be safe in this case
			result->data = (unsigned char*)data;
		}
		chunk->size += actual_subchunk_size(header_data);
	}
	else
	{
		result->data = NULL;
	}
	chunk->size += 4;
	return result;
}

spc_tag_xid666 *spc_tag_xid666_new(void)
{
	spc_tag_xid666 *result = malloc(sizeof(*result));
	spc_tag_xid666_constructor(result);
	return result;
}

void spc_tag_xid666_free(spc_tag_xid666 *chunk)
{
	spc_tag_xid666_destructor(chunk);
	free(chunk);
}

void spc_tag_xid666_constructor(spc_tag_xid666 *chunk)
{
	memset(chunk, 0, sizeof(*chunk));
	chunk->capacity = 5;
	chunk->data = calloc(chunk->capacity, sizeof(*chunk->data));
}

void spc_tag_xid666_destructor(spc_tag_xid666 *chunk)
{
	if (!chunk)
		return;

	for (size_t i = 0; i < chunk->nchunks; ++i)
	{
		spc_tag_xid666_data *sub = &chunk->data[i];
		if (sub->data)
			free(sub->data);
	}
	if (chunk->data)
		free(chunk->data);
	chunk->nchunks = 0;
	chunk->capacity = 0;
	chunk->data = NULL;
}

spc_tag_error spc_tag_xid666_parse_file(FILE *file, spc_tag_xid666 *chunk)
{
	if (!file || !chunk)
		return SPC_TAG_IO_ERR;

	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_xid666_parse(&accessor, chunk);
}

spc_tag_error spc_tag_xid666_parse(
	spc_tag_accessor *accessor, spc_tag_xid666 *chunk)
{
	if (!accessor ||
		spc_tag_accessor_seek(accessor, 0x10200, SEEK_SET) ||
		spc_tag_accessor_read(accessor, chunk->type, 4) != 4)
	{
		return SPC_TAG_IO_ERR;
	}

	if (strncmp(chunk->type, "xid6", 4))
		return SPC_TAG_PARSE_ERR;

	unsigned char raw[4];
	if (spc_tag_accessor_read(accessor, raw, 4) != 4)
		return false;
	uint32_t chunk_size = raw[0] | raw[1] << 8 | raw[2] << 16 | raw[3] << 24;
	chunk->size = 0; // This will be updated by data_parse
	chunk->nchunks = 0;

	// Size must be a multiple of 4 due to 4 byte alignment of subchunks
	if (chunk_size % 4)
		return SPC_TAG_PARSE_ERR;

	for (uint32_t size = chunk_size; size; size -= 4)
	{
		spc_tag_xid666_data *sub = spc_tag_xid666_data_parse(accessor, chunk);

		// Just bail if data_parse fails. Chunk should be in an incomplete but
		// consistent state.
		if (!sub)
			return SPC_TAG_PARSE_ERR;

		if (sub->type)
		{
			uint32_t subchunk_size = actual_subchunk_size(sub->header_data);
			// Bail if the amount of data that's supposed to be left is less
			// than what we just parsed.
			if (size < subchunk_size)
				return SPC_TAG_PARSE_ERR;
			size -= subchunk_size;
		}
	}

	return chunk->size == chunk_size ? SPC_TAG_SUCCESS : SPC_TAG_PARSE_ERR;
}

spc_tag_xid666_data *spc_tag_xid666_get_data(
	spc_tag_xid666 *xid666, size_t index)
{
	if (index < xid666->nchunks)
		return xid666->data + index;
	return NULL;
}

size_t spc_tag_xid666_get_size(const spc_tag_xid666 *xid666)
{
	return xid666->size;
}

size_t spc_tag_xid666_get_chunks(const spc_tag_xid666 *xid666)
{
	return xid666->nchunks;
}

spc_tag_xid666_data* spc_tag_xid666_data_parse_file(
	FILE* file, spc_tag_xid666* chunk)
{
	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_xid666_data_parse(&accessor, chunk);
}

spc_tag_xid666_data* spc_tag_xid666_data_parse(
	spc_tag_accessor *accessor, spc_tag_xid666* chunk)
{
	// Read 4 bytes unconditinally (well, unless there are no more to read),
	// then decide whether the header is valid
	unsigned char id, type;
	if (spc_tag_accessor_read(accessor, &id, 1) != 1 ||
		spc_tag_accessor_read(accessor, &type, 1) != 1)
	{
		return NULL;
	}

	unsigned char raw[2];
	if (spc_tag_accessor_read(accessor, raw, 2) != 2)
		return NULL;
	uint16_t header_data = raw[0] | raw[1] << 8;

	// Now, check if the header is valid and bail if it isn't
	if (!is_id_valid(id))
		return NULL;

	// No point in checking data, as we will allocate it if it is necessary
	if (!sanity_check_type(type, header_data))
		return NULL;

	unsigned char* data = NULL;
	if (type)
	{
		data = malloc(header_data);
		if (spc_tag_accessor_read(accessor, data, header_data) != header_data)
		{
			free(data);
			return NULL;
		}

		// Data must be null terminated for String type
		if (type == 0x01 && data[header_data-1])
		{
			free(data);
			return NULL;
		}

		// Make sure we leave file pointer in a 4 byte aligned position
		size_t padding = subchunk_padding(header_data);
		if (padding)
		{
			spc_tag_accessor_seek(accessor, padding, SEEK_CUR);
		}
	}

	// Move data into new subchunk using copy=false parameter
	spc_tag_xid666_data *sub =
		spc_tag_xid666_add_data_(chunk, id, header_data, data, false);

	// If we can't add a chunk, remember to free data field we allocated, since
	// the helper function doesn't do this for us in case of a failure
	if (!sub)
		free(data);

	return sub;
}

// FIXME what about updating the chunk size?
spc_tag_xid666_data* spc_tag_xid666_data_update(
	spc_tag_xid666_data* subchunk,
	uint8_t id,
	uint16_t header_data,
	const unsigned char* data)
{
	if (!is_id_valid(id))
		return NULL;

	uint8_t type = get_type_from_id(id);
	if (!sanity_check_type_data(type, header_data, data))
		return NULL;

	if (type)
	{
		unsigned char *new_data = realloc(subchunk->data, header_data);
		if (!new_data)
			return NULL;
		subchunk->data = new_data;
		memcpy(subchunk->data, data, header_data);
	}
	else
	{
		free(subchunk->data);
		subchunk->data = NULL;
	}
	subchunk->id = id;
	subchunk->type = type;
	subchunk->header_data = header_data;

	return subchunk;
}

spc_tag_xid666_data* spc_tag_xid666_add_data(
	spc_tag_xid666* chunk,
	uint8_t id,
	uint16_t header_data,
	const unsigned char* data)
{
	return spc_tag_xid666_add_data_(chunk, id, header_data, data, true);
}

spc_tag_error spc_tag_xid666_write_file(
	FILE *file, const spc_tag_xid666 *xid666)
{
	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_xid666_write(&accessor, xid666);
}

spc_tag_error spc_tag_xid666_write(
	spc_tag_accessor *accessor, const spc_tag_xid666 *xid666)
{
	// Skip to the xid666 chunk header
	spc_tag_accessor_seek(accessor, 0x10200, SEEK_SET);
	if (spc_tag_accessor_write(accessor, "xid6", 4) != 4)
		return SPC_TAG_IO_ERR;

	unsigned char raw[4];
	// FIXME should we calculate the size as we write? Should we even store the
	// size while the structure is in memory? The problem comes from the
	// update() function... it's difficult to update the size of an existing
	// element. If we just recalculate the size when we either ask for it, or
	// when writing to disk, we're guaranteed to be correct, regardless of how
	// much was modified.
	raw[3] = xid666->size >> 24;
	raw[2] = xid666->size >> 16;
	raw[1] = xid666->size >> 8;
	raw[0] = xid666->size;
	if (spc_tag_accessor_write(accessor, raw, 4) != 4)
		return SPC_TAG_IO_ERR;

	for (size_t i = 0; i < xid666->nchunks; ++i)
	{
		spc_tag_error result =
			spc_tag_xid666_data_write(accessor, xid666->data + i);
		if (result != SPC_TAG_SUCCESS)
			return result;
	}
	return SPC_TAG_SUCCESS;
}

spc_tag_error spc_tag_xid666_data_write_file(
	FILE *file, const spc_tag_xid666_data *data)
{
	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_xid666_data_write(&accessor, data);
}

spc_tag_error spc_tag_xid666_data_write(
	spc_tag_accessor *accessor, const spc_tag_xid666_data *data)
{
	unsigned char raw[4];
	raw[3] = data->header_data >> 8;
	raw[2] = data->header_data;
	raw[1] = data->type;
	raw[0] = data->id;
	if (spc_tag_accessor_write(accessor, raw, 4) != 4)
		return SPC_TAG_IO_ERR;

	if (data->type)
	{
		if(spc_tag_accessor_write(accessor, data->data, data->header_data) !=
				data->header_data)
			return SPC_TAG_IO_ERR;
		// Make sure we leave file pointer in a 4 byte aligned position
		size_t padding = subchunk_padding(data->header_data);
		if (padding)
		{
			spc_tag_accessor_seek(accessor, padding, SEEK_CUR);
		}
	}
	return SPC_TAG_SUCCESS;
}

uint8_t spc_tag_xid666_data_get_id(const spc_tag_xid666_data *data)
{
	return data->id;
}

uint8_t spc_tag_xid666_data_get_type(const spc_tag_xid666_data *data)
{
	return data->type;
}

uint16_t spc_tag_xid666_data_get_header_data(const spc_tag_xid666_data *data)
{
	return data->header_data;
}

unsigned char* spc_tag_xid666_data_get_data(const spc_tag_xid666_data *data)
{
	return data->data;
}
