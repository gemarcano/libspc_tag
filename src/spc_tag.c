// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <spc_tag/spc_tag.h>
#include <spc_tag/spc_tag_file_accessor.h>
#include <spc_tag/spc_tag_memory_accessor.h>
#include <spc_tag/spc_tag_accessor.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>

struct spc_tag
{
	/** Container for all ID666 and XID666 metadata */
	spc_tag_metadata *metadata;
	/** Value of PC SPC-700 register */
	uint16_t pc;
	/** Value of A SPC-700 register */
	uint8_t a;
	/** Value of X SPC-700 register */
	uint8_t x;
	/** Value of Y SPC-700 register */
	uint8_t y;
	/** Value of PSW SPC-700 register */
	uint8_t psw;
	/** Value of lower byte of SP SPC-700 register */
	uint8_t sp;
	uint16_t reserved;
	/** RAM contents of SPC-700 */
	unsigned char ram[0x10000];
	/** Values of DSP registers */
	unsigned char dsp_regs[128];
	/** Contents of extra RAM */
	unsigned char extra_ram[64];
};

void spc_tag_constructor(spc_tag *data)
{
	if (data == NULL)
		return;

	*data = (spc_tag){0};
	data->metadata = spc_tag_metadata_new();
}

void spc_tag_destructor(spc_tag *data)
{
	if (data == NULL)
		return;

	spc_tag_metadata_free(data->metadata);
	data->metadata = NULL;
}

spc_tag *spc_tag_new(void)
{
	spc_tag *data = malloc(sizeof(*data));

	spc_tag_constructor(data);
	return data;
}

void spc_tag_free(spc_tag *data)
{
	if (data == NULL)
		return;
	spc_tag_destructor(data);
	free(data);
}

spc_tag_error spc_tag_parse_file(FILE *file, spc_tag *data)
{
	if (!file)
		return SPC_TAG_IO_ERR;

	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_parse(&accessor, data);
}

spc_tag_error spc_tag_parse_memory(unsigned char *memory, size_t size, spc_tag *data)
{
	if (!memory)
		return SPC_TAG_IO_ERR;

	spc_tag_accessor accessor;
	spc_tag_memory mem;
	spc_tag_memory_constructor(&mem, size);
	memcpy(mem.data, memory, size);
	spc_tag_memory_accessor_init(&accessor, &mem);
	return spc_tag_parse(&accessor, data);
}

spc_tag_error spc_tag_parse(spc_tag_accessor *accessor, spc_tag *data)
{
	if (!accessor || !data)
		return SPC_TAG_IO_ERR;

	spc_tag_error result = SPC_TAG_SUCCESS;
	result = spc_tag_metadata_parse(accessor, data->metadata);
	if (result != SPC_TAG_SUCCESS)
		return result;

	spc_tag_accessor_seek(accessor, 0x25, SEEK_SET);
	if (spc_tag_accessor_read(accessor, &data->pc, 2) != 2 ||
		spc_tag_accessor_read(accessor, &data->a, 1) != 1 ||
		spc_tag_accessor_read(accessor, &data->x, 1) != 1 ||
		spc_tag_accessor_read(accessor, &data->y, 1) != 1 ||
		spc_tag_accessor_read(accessor, &data->psw, 1) != 1 ||
		spc_tag_accessor_read(accessor, &data->sp, 1) != 1 ||
		spc_tag_accessor_read(accessor, &data->reserved, 2) != 2)
	{
		return SPC_TAG_IO_ERR;
	}

	spc_tag_accessor_seek(accessor, 0x100, SEEK_SET);
	if (spc_tag_accessor_read(accessor, data->ram, 0x10000) != 0x10000 ||
		spc_tag_accessor_read(accessor, data->dsp_regs, 128) != 128)
	{
		return SPC_TAG_IO_ERR;
	}

	spc_tag_accessor_seek(accessor, 0x101C0, SEEK_SET);
	if (spc_tag_accessor_read(accessor, data->extra_ram, 64) != 64)
		return SPC_TAG_IO_ERR;

	return SPC_TAG_SUCCESS;
}

spc_tag_error spc_tag_write_file(FILE *file, const spc_tag* data)
{
	if (!file)
		return SPC_TAG_IO_ERR;
	spc_tag_accessor accessor;
	spc_tag_file_accessor_init(&accessor, file);
	return spc_tag_write(&accessor, data);
}

spc_tag_error spc_tag_write(spc_tag_accessor *accessor, const spc_tag* data)
{
	if (!data || !accessor)
		return SPC_TAG_IO_ERR;

	// Header is written by metadata write function, so just write data
	// metadata doesn't handle
	spc_tag_accessor_seek(accessor, 0x25, SEEK_SET);
	unsigned char raw[2];
	raw[1] = data->pc >> 8;
	raw[0] = data->pc;
	if (spc_tag_accessor_write(accessor, raw, 2) != 2 ||
		spc_tag_accessor_write(accessor, &data->a, 1) != 1 ||
		spc_tag_accessor_write(accessor, &data->x, 1) != 1 ||
		spc_tag_accessor_write(accessor, &data->y, 1) != 1 ||
		spc_tag_accessor_write(accessor, &data->psw, 1) != 1 ||
		spc_tag_accessor_write(accessor, &data->sp, 1) != 1)
	{
		return SPC_TAG_IO_ERR;
	}
	raw[1] = data->reserved >> 8;
	raw[0] = data->reserved;
	if (spc_tag_accessor_write(accessor, &raw, 2) != 2)
		return SPC_TAG_IO_ERR;

	spc_tag_accessor_seek(accessor, 0x100, SEEK_SET);
	if (spc_tag_accessor_write(accessor, &data->ram, 0x10000) != 0x10000 ||
		spc_tag_accessor_write(accessor, &data->dsp_regs, 128) != 128)
	{
		return SPC_TAG_IO_ERR;
	}
	spc_tag_accessor_seek(accessor, 0x101C0, SEEK_SET);
	if (spc_tag_accessor_write(accessor, &data->extra_ram, 64) != 64)
		return SPC_TAG_IO_ERR;

	return spc_tag_metadata_write(accessor, data->metadata);
}

#define DEFINE_TAG_GETSET(TYPE, NAME) \
TYPE spc_tag_get_ ## NAME(const spc_tag *data) \
{ \
	return data->NAME; \
} \
spc_tag_error spc_tag_set_ ## NAME(spc_tag *data, TYPE value) \
{ \
	data->NAME = value; \
	return SPC_TAG_SUCCESS; \
}

#define DEFINE_TAG_GETSET_ARRAY(TYPE, NAME) \
TYPE spc_tag_get_ ## NAME(const spc_tag *data) \
{ \
	return data->NAME; \
} \
spc_tag_error spc_tag_set_ ## NAME(spc_tag *data, TYPE value, size_t length) \
{ \
	static_assert(sizeof(data->NAME) > sizeof(void*), \
		"metadata fields appears to be a pointer and not an array!"); \
	if (length > sizeof(data->NAME)) \
		return SPC_TAG_PARSE_ERR; \
	memcpy(data->NAME, value, length); \
	return SPC_TAG_SUCCESS; \
}

DEFINE_TAG_GETSET(uint16_t, pc)
DEFINE_TAG_GETSET(uint8_t, a)
DEFINE_TAG_GETSET(uint8_t, x)
DEFINE_TAG_GETSET(uint8_t, y)
DEFINE_TAG_GETSET(uint8_t, psw)
DEFINE_TAG_GETSET(uint8_t, sp)
DEFINE_TAG_GETSET(uint16_t, reserved)
DEFINE_TAG_GETSET_ARRAY(const unsigned char *, ram)
DEFINE_TAG_GETSET_ARRAY(const unsigned char *, dsp_regs)
DEFINE_TAG_GETSET_ARRAY(const unsigned char *, extra_ram)

#define DEFINE_META_GETSET(RET, NAME) \
RET spc_tag_get_ ## NAME(const spc_tag *data) \
{ \
	return spc_tag_metadata_get_##NAME(data->metadata); \
} \
spc_tag_error spc_tag_set_##NAME(spc_tag *data, RET value) \
{ \
	return spc_tag_metadata_set_##NAME(data->metadata, value); \
}

#define DEFINE_META_GETSET_ARRAY(RET, NAME) \
RET spc_tag_get_ ## NAME(const spc_tag *data) \
{ \
	return spc_tag_metadata_get_##NAME(data->metadata); \
} \
spc_tag_error spc_tag_set_##NAME(spc_tag *data, RET value, size_t length) \
{ \
	return spc_tag_metadata_set_##NAME(data->metadata, value, length); \
}

DEFINE_META_GETSET(bool, has_id666)
DEFINE_META_GETSET(uint8_t, minor_version)
DEFINE_META_GETSET(bool, binary)
DEFINE_META_GETSET_ARRAY(const char *, song_title)
DEFINE_META_GETSET_ARRAY(const char *, game_title)
DEFINE_META_GETSET_ARRAY(const char *, name_of_dumper)
DEFINE_META_GETSET_ARRAY(const char *, comments)
DEFINE_META_GETSET(struct tm, dump_date)
DEFINE_META_GETSET(unsigned, seconds_until_fade)
DEFINE_META_GETSET(unsigned, fade_length_ms)
DEFINE_META_GETSET_ARRAY(const char *, song_artist)
DEFINE_META_GETSET(bool, default_channel_disables)
DEFINE_META_GETSET(uint8_t, emulator_used_to_dump)
DEFINE_META_GETSET(bool, has_xid666)

spc_tag_xid666 *spc_tag_get_xid666(spc_tag *data)
{
	return spc_tag_metadata_get_xid666(data->metadata);
}
