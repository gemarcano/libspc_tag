// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <spc_tag/spc_tag.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

const char usage_str[] = "Usage: %s [spc-files...]\n";

void usage(const char *file)
{
	printf(usage_str, file);
}

static const char* id_to_name(uint8_t id)
{
	switch (id)
	{
		case 0x01: return "song name";
		case 0x02: return "game name";
		case 0x03: return "artist name";
		case 0x04: return "name of dumper";
		case 0x05: return "dump date";
		case 0x06: return "emulator used to dump";
		case 0x07: return "comments";
		case 0x10: return "official soundtrack title";
		case 0x11: return "OST disk";
		case 0x12: return "OST track";
		case 0x13: return "publisher";
		case 0x14: return "copyright year";
		case 0x30: return "introduction length";
		case 0x31: return "loop length";
		case 0x32: return "end length";
		case 0x33: return "fade length";
		case 0x34: return "muted voices";
		case 0x35: return "number of times to loop";
		case 0x36: return "mixing level";
		default: return "unknown field";
	}
}

static int process_file(FILE *file)
{
	spc_tag *tag =	spc_tag_new();
	if (spc_tag_parse_file(file, tag))
	{
		fprintf(stderr, "Failed to parse given SPC file!\n");
		return -1;
	}
	if (!spc_tag_get_has_id666(tag))
	{
		fprintf(stderr, "This SPC file does not have id666 data.");
		return 0;
	}

	const char *json = "\t{\n"
		"\t\t\"minor version\": %"PRIu8",\n"
		"\t\t\"song title\": \"%s\",\n"
		"\t\t\"game title\": \"%s\",\n"
		"\t\t\"name of dumper\": \"%s\",\n"
		"\t\t\"comments\": \"%s\",\n"
		"\t\t\"dump date\": \"%s\",\n"
		"\t\t\"seconds until fade\": %u,\n"
		"\t\t\"fade length (ms)\": %u,\n"
		"\t\t\"song artist\": \"%s\",\n"
		"\t\t\"default channel disables\": %d,\n"
		"\t\t\"emulator used to dump\": %"PRIu8;

	struct tm date = spc_tag_get_dump_date(tag);
	char date_str[11];
	strftime(date_str, 11, "%F", &date);

	printf(json,
		spc_tag_get_minor_version(tag),
		spc_tag_get_song_title(tag),
		spc_tag_get_game_title(tag),
		spc_tag_get_name_of_dumper(tag),
		spc_tag_get_comments(tag),
		date_str,
		spc_tag_get_seconds_until_fade(tag),
		spc_tag_get_fade_length_ms(tag),
		spc_tag_get_song_artist(tag),
		spc_tag_get_default_channel_disables(tag),
		spc_tag_get_emulator_used_to_dump(tag)
	);

	if (spc_tag_get_has_xid666(tag))
	{
		spc_tag_xid666 *chunk = spc_tag_get_xid666(tag);
		size_t chunks = spc_tag_xid666_get_chunks(chunk);
		if (chunks)
			printf(",\n\t\t\"xid666\": {\n");

		for (size_t i = 0; i < chunks; ++i)
		{
			spc_tag_xid666_data *sub = spc_tag_xid666_get_data(chunk, i);
			unsigned char *data = spc_tag_xid666_data_get_data(sub);
			uint8_t id = spc_tag_xid666_data_get_id(sub);
			uint8_t type = spc_tag_xid666_data_get_type(sub);
			printf("\t\t\t\"%s\": ", id_to_name(id));
			switch(type)
			{
				case 0x00:
					printf("%"PRIu16, spc_tag_xid666_data_get_header_data(sub));
					break;
				case 0x01:
					printf("\"%s\"", (const char*)data);
					break;
				case 0x04:
				{
					uint32_t integer =
						data[0] | data[1] << 8 | data[2] << 16 | data[3] << 24;
					printf("%"PRIu32, integer);
					break;
				}
			}
			if (i + 1 != chunks)
				printf(",");
			printf("\n");
		}
		if (chunks)
			printf("\t\t}");
	}
	printf("\n\t}");
	spc_tag_free(tag);

	return 0;
}

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		// This is extremely unlikely, but cppcheck is complaining about it.
		// Per the C standard, argc of 0 is possible, and argv[0] is NULL
		const char *prog = argc ? argv[0] : "spc_tag";
		usage(prog);
		return -1;
	}

	printf("[\n");
	bool previous = false;
	for (size_t i = 0; i < argc - 1; ++i)
	{
		FILE* file = fopen(argv[i+1], "rb");
		if (!file)
		{
			fprintf(stderr, "\nFile \"%s\" not found!\n", argv[i+1]);
			continue;
		}

		if (previous)
		{
			printf(",");
			printf("\n");
		}
		process_file(file);
		fflush(stdout);
		fclose(file);
		previous = true;
	}
	printf("\n]\n");
	return 0;
}
