// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <spc_tag/spc_tag_accessor.h>

size_t spc_tag_accessor_read(spc_tag_accessor *accessor, void *dest, size_t amount)
{
	if (accessor->read)
		return accessor->read(accessor->data, dest, amount);
	return 0;
}

size_t spc_tag_accessor_write(spc_tag_accessor *accessor, const void *src, size_t amount)
{
	if (accessor->write)
		return accessor->write(accessor->data, src, amount);
	return 0;
}

int spc_tag_accessor_seek(spc_tag_accessor *accessor, long pos, int flags)
{
	if (accessor->seek)
		return accessor->seek(accessor->data, pos, flags);
	return -1;
}
