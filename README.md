# libspc_tag

libspc_tag is a library for read and writing SNES SPC-700 .spc dumps. It is
written in C11.

## Features
 - Reading .spc files in their entirety (spc_tag), or just the metadata
   (spc_tag_metadata)
 - xid666 support (including adding new tags)
 - Writing .spc files

## Build dependencies
 - Make
 - C11 compiler or newer

The following are needed if building from the repository and not from a source
release:
 - Autotools (Autoconf, Automake, Libtool)
 - autoconf-archive

## Building and installing

For a source release:
```
mkdir build && cd build
../configure
make && make install
```

If using source code from the repository:
```
autoreconf -if
mkdir build && build
../configure
make && make install
```

See `../configure --help` to see what parameters and options can be controlled.
By default, the install directory prefix is set to `/usr/local`, but this can
be modified by using the `--prefix` argument to `../configure`.

## Running tests
```
# From the build directory after building
make check
```

## Generating documentation

The header files are documented using Doxygen syntax. To generate the Doxygen
documentation, run:
```
# From the root directory of the repository/source archive
doxygen
```

The documentation will be written to the `documentation/` directory.
